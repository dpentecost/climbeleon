﻿using UnityEngine;
using System.Collections;

public class BackgroundEmitterController : MonoBehaviour {

	//The prefab
	public GameObject backgroundEmitterPrefab;

	//Are we the main menu?
	public bool mainMenu = false;

	//Where to place things
	private float normalZ = 30f;
	private float mainZ = 1f;

	// Use this for initialization
	void Start () {

		//Where do we want to put them?
		Vector3 newPosition = this.transform.localPosition;

		//Set it to be above the camera
		Camera camera = this.GetComponent<Camera>();
		newPosition.y = camera.orthographicSize * 1.1f;

		//Z position depending on where we are
		if (this.mainMenu)
		{
			//Position for main menu
			newPosition.z = this.mainZ;
		}
		else
		{
			//Position for the regular game
			newPosition.z = 30f;
		}

		//We want to create a particle emitter for each color
		ColorHelper.COLORS_ENUM[] colors = new ColorHelper.COLORS_ENUM[] {
			ColorHelper.COLORS_ENUM.COLOR1, ColorHelper.COLORS_ENUM.COLOR2, ColorHelper.COLORS_ENUM.COLOR3, ColorHelper.COLORS_ENUM.SAFE
		};

		//Are we color blind?
		bool colorblind = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_COLORBLIND);

		foreach(ColorHelper.COLORS_ENUM color in colors)
		{
			//We want to instantiate a new emitter
			GameObject emitter = (GameObject)Instantiate(backgroundEmitterPrefab, transform.position, Quaternion.identity);

			//We want to set its start color
			emitter.GetComponent<ParticleSystem>().startColor = ColorHelper.GetColorFromEnum(color, colorblind);

			//Also it should follow above
			emitter.transform.SetParent(this.transform, false);
			emitter.transform.localPosition = newPosition;
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
