﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	//Who are we following?
	public GameObject targetGameObject;

	//Are we following?
	public bool isFollowing = true;

	//Minimum camera height
	private float targetY = 0;

	// Use this for initialization
	void Start () {

		//Use the background color
		Camera camera = this.gameObject.GetComponent<Camera>();
		camera.backgroundColor = Color.black;//ColorHelper.colorBackground;

		//Setup the background image
		GameObject backgroundImage = this.transform.Find("BackgroundImage").gameObject;

		//Set its color
		backgroundImage.GetComponent<SpriteRenderer>().color = ColorHelper.colorBackground;

		//And Y scale
		Vector3 oldScale = backgroundImage.transform.localScale;
		float yScale = camera.orthographicSize * 2.5f;
		oldScale.y = yScale;
		backgroundImage.transform.localScale = oldScale;
	}
	
	// Update is called once per frame
	void Update () {

		//Are we following?
		if (this.isFollowing)
		{

			//Follow that game object!
			//Get the target y
			this.targetY = targetGameObject.transform.position.y;

			//Did it go too far?
			if (this.targetY <= ToolboxSingleton.Instance.MIN_CAMERA_POSISITON)
			{
				//Stop there
				this.targetY = ToolboxSingleton.Instance.MIN_CAMERA_POSISITON;
			}
		}

		//Now get the new height
		float currentY = Mathf.Lerp(this.transform.position.y, this.targetY, ToolboxSingleton.Instance.CAMERA_DAMPING_FACTOR * Time.deltaTime);

		//Set the height of this camera
		this.transform.position = new Vector3(this.transform.position.x, currentY, this.transform.position.z);
	}
}
