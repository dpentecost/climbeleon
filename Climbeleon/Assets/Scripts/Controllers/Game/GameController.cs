﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

	//The active player and camera
	public Camera playerCamera;

	//The current score
	private int score = 0;

	//The seed we're using
	public bool useSeed = false;
	public int seed = 0;

	//What difficulty level are we at?
	public int difficulty = 0;
	public static readonly int DIFFICULTY_TUTORIAL = 0;
	public static readonly int DIFFICULTY_BEGINNER = 1;
	public static readonly int DIFFICULTY_EASY = 2;
	public static readonly int DIFFICULTY_MEDIUM = 3;
	public static readonly int DIFFICULTY_HARD = 4;
	public static readonly int DIFFICULTY_EVIL = 5;
	public static readonly int DIFFICULTY_IMPOSSIBLE = 6;
	public static readonly int MAX_DIFFICULTY = 6;

	//How did the user die?
	public bool playerDead = false;
	public static readonly int DEATH_BLACK = 0;
	public static readonly int DEATH_LASER = 1;
	public static readonly int DEATH_COLOR = 2;

	//The time to restart again
	public float restartTime = 1;

	//First things first lets get the RNG going
	void Awake()
	{
		//Set the seed if needed
		if (!this.useSeed)
		{
			//We want to get one!
			this.seed = (int)System.DateTime.Now.Ticks;
		}

	}

	// Use this for initialization
	void Start () {	

		//Use the appropriate bgm
		AudioController.Instance.LoopBGM(AudioController.Instance.gameBGM);

		//Increase the play count
		ToolboxSingleton.Instance.IncrementPlayCount();
	}

	//Increment the score
	public void IncrementScore(int amount = 1)
	{
		//Just increase it by one unless we really need more... unless we're dead
		if(!this.playerDead)
			this.score += amount;
	}

	//Simply get the score
	public int GetScore()
	{
		//Return it
		return this.score;
	}

	//For restarting
	public void RestartGame()
	{
		//Save user settings because if we don't save often it can get missed.
		SettingsAndControls.Save();

#if UNITY_ANDROID || UNITY_IPHONE
		//Is the ad ready?
		if (AdvertisementsSingleton.Instance.InterstitialIsReady() && ToolboxSingleton.Instance.PlayerIsReadyForAdvertisements())
		{
			//Add ourselves to the closed
			//Make sure we're notified when it's done
			AdvertisementsSingleton.Instance.AddOnAdClosed(this.HandleOnAdClosed);

			//Fade and display it
			StartCoroutine(this.DisplayInterstitialAd());
		}
		else
		{
			//Go straight to restarting
			ToolboxSingleton.Instance.FadeToScene(ToolboxSingleton.SCENE_GAME);
		}
#else
		//We are not on mobile so we will only restart
		ToolboxSingleton.Instance.FadeToScene(ToolboxSingleton.SCENE_GAME);
#endif
	}

	//Display the interstitial
	private IEnumerator DisplayInterstitialAd()
	{
		//Fade to black...
		yield return ToolboxSingleton.Instance.FadeToBlack();

		//We want to display it
		AdvertisementsSingleton.Instance.DisplayInterstitialAd();
	}

	//Restarting with an ad
	public void HandleOnAdClosed(object sender, EventArgs args)
	{
		//We restart
		ToolboxSingleton.Instance.FadeToScene(ToolboxSingleton.SCENE_GAME);
	}

	//For quitting
	public void QuitGame()
	{
		//Go to the main menu
		ToolboxSingleton.Instance.FadeToScene(ToolboxSingleton.SCENE_MAIN);

		//Destroy this so we don't quit like a million times.
		//TODO: Maybe just use a flag? 
		Destroy(this);
	}
	
	// Update is called once per frame
	void Update () {

		//Every update, we decrease the time
		restartTime -= Time.deltaTime;
		if (restartTime < 0)
		{
			//Stay there
			restartTime = 0;
		}

		//Did the user hit the restart key? Did we wait?
		if (ControllerHelper.GetRestartAxis() > 0 && restartTime <= 0)
		{
			//We restart also
			restartTime = 100;
			this.RestartGame();
		}

		//Did the player hit the quit button (escape)?
		if(ControllerHelper.GetExitAxis() > 0 && restartTime <= 0)
		{
			//We're quitting
			this.QuitGame();
		}


		//Update the difficulty text
		if (ToolboxSingleton.DEBUG)
		{
			Text difficultyText = GameObject.Find("DifficultyText").GetComponent<Text>();
			difficultyText.text = "D: " + this.difficulty;
		}	
	}

	//The player died
	public void PlayerKilled(int reason)
	{
		//No more player
		this.playerDead = true;

		//We want to let the camera know the player died
		this.playerCamera.GetComponent<CameraController>().isFollowing = false;

		//Move the restart button in the UI
		GameUIController gameUI = GameObject.Find("GameUI").GetComponent<GameUIController>();
		gameUI.ShowGameOverPanel();

		//Tell the network controller that we want to submit the score
		bool newHighScore = SocialSingleton.Instance.ReportScore(this.score);

		//Did we get a new high score? We gotta get some fanfare up here
		//TODO: DO THAT

		//We died so handle that
		SocialSingleton.Instance.IncrementFallAchievement();

		//We climbed some
		SocialSingleton.Instance.IncrementClimbAchievement(this.score);
		SocialSingleton.Instance.IncrementClimbEvent(this.score);

		//How did we die?
		if (reason == DEATH_BLACK)
		{
			//We hit a black tile
			SocialSingleton.Instance.IncrementBlackTileAchievement();			
		}else if(reason == DEATH_LASER)
		{
			//Got zapped
			SocialSingleton.Instance.IncrementLaserAchievement();
		}else if(reason == DEATH_COLOR)
		{
			//Mis-match
			SocialSingleton.Instance.IncrementColorAchievement();
		}

		//Always increment the fall event
		SocialSingleton.Instance.IncrementFallEvent();
	}
}
