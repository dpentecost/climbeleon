﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{

	//The master game controller
	public GameObject gameController;

	//The player input controller. We better have one
	public PlayerInputController playerInputController;

	//The bits and pieces when hitting a laser
	public GameObject playerBitPrefab;

	//Which direction are we jumping?
	private bool jumpLeft = false;
	private bool canJump = true;
	private bool jumping = false;
	private bool dead = false;
	private bool canPrejump = false;
	private bool prejumping = false;
	private float prejumpTime = 0.11f;
	private bool prejumpEnabled = false;

	//Invincibility buffer when changing the color before jumping
	private bool invincible = false;
	private bool canInvincible = false;
	private float invincibilityBufferTime = 0.25f; //In seconds

	//Are we touching a wall?
	private bool touchingWall = false;

	//Colors. Should be able to remap these eventually
	public ColorHelper.COLORS_ENUM currentColor = ColorHelper.COLORS_ENUM.SAFE;

	//Which colors are pressed during quickjump?
	public HashSet<ColorHelper.COLORS_ENUM> activeColorButtons = new HashSet<ColorHelper.COLORS_ENUM>();

	//Varaiables for jumping
	public float jumpImpulseX = 20;
	public float jumpImpulseY = 17;
	private float jumpImpulseXBoost = 20;
	private float jumpImpulseYBoost = 19;

	//Variables for crashing
	private float crashImpulseXMin = 5;
	private float crashImpulseXMax = 10;
	private float crashImpulseYMin = 2;
	private float crashImpulseYMax = 5;
	private float crashImpulseZMin = 10;
	private float crashImpulseZMax = 15;

	//Variables for exploding into laser
	private int playerBitCount = 5;
	private bool burned = false;

	//Physics materials
	public PhysicsMaterial2D aliveMaterial;
	public PhysicsMaterial2D crashMaterial;

	//Default for gravity
	private float gravity = 1;
	private float gravityMore = 8;

	//Number of particles to emit
	private int colorParticles = 20;
	private int jumpParticles = 15;
	private int boostParticles = 25;

	// Use this for initialization
	void Start()
	{
		//Look for the game controller
		if (!this.gameController)
		{
			//Search by tag
			this.gameController = GameObject.FindGameObjectWithTag(ToolboxSingleton.TAG_GAMECONTROLLER);
		}

		//Get the input controller component
		this.playerInputController = this.gameObject.GetComponent<PlayerInputController>();

		//Start as the safe color
		this.gameObject.GetComponent<SpriteRenderer>().color = ColorHelper.colorSafe;
	}

	// Update is called once per frame
	void Update()
	{

	}

	//When we press the colors
	public void ColorEntered(ColorHelper.COLORS_ENUM color)
	{
		//Change the color
		this.ChangeColor(color);
	}

	//When we release the colors
	public void ColorExited(ColorHelper.COLORS_ENUM color)
	{
		//Don't actually do anything I suppose
	}

	//When we press the colors
	public void ColorPressed(ColorHelper.COLORS_ENUM color)
	{
		//This is where we used to do the quickjump code
	}

	//When we release the colors
	public void ColorReleased(ColorHelper.COLORS_ENUM color)
	{
		//This is where we used to quickjump stuff
	}

	//When the user hits the jump button
	public void JumpButtonDown()
	{
		//No matter what decrease gravity
		this.JumpMore();

		//Can we jump?
		this.TryJump();
	}

	//When the user releases the jump button
	public void JumpButtonReleased()
	{
		//Try to increase gravity
		if (this.AllJumpInputsReleased())
		{
			//Yep, we can fall
			this.JumpLess();
		}
	}

	//Check all the jump buttons to see if they are all released
	public bool AllJumpInputsReleased()
	{
		//We used to check quickjump stuff
		//But we just don't care now
		return true;
	}

	public void TryJump()
	{
		//Try to jump. Return if successful
		if (this.canJump && !this.dead)
		{
			//Ok, lets jump
			this.Jump();
			return;
		}
		else
		{
			//Ok, can we pre-jump?
			if (this.canPrejump && this.prejumpEnabled)
			{
				//We prejump
				StartCoroutine(PreJump(this.prejumpTime));
			}
		}
	}

	private IEnumerator PreJump(float time)
	{
		//We can no longer prejump since we are now doing so
		this.canPrejump = false;
		this.prejumping = true;

		//Wait for the timer to end
		yield return new WaitForSeconds(time);

		//We are no longer prejumping
		this.prejumping = false;
	}

	//For changing color
	void ChangeColor(ColorHelper.COLORS_ENUM targetColor)
	{
		//We cannot change color when we are dead
		if (this.dead) return;

		//Are we already this color?
		if (!this.currentColor.Equals(targetColor))
		{
			//Get the actual color
			bool colorBlind = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_COLORBLIND);
			Color actualTargetColor = ColorHelper.GetColorFromEnum(targetColor, colorblind: colorBlind);

			//Now change color
			SpriteRenderer spriteRenderer = this.GetComponent<SpriteRenderer>();
			spriteRenderer.color = actualTargetColor;
			this.currentColor = targetColor;

			//Also change the color of the particle emitter
			ParticleSystem particleSystem = this.GetComponent<ParticleSystem>();
			particleSystem.startColor = actualTargetColor;
			particleSystem.Emit(this.colorParticles);

			//Get the emitter of the child
			GameObject trailParticleEmitter = this.transform.Find("ParticleTrail").gameObject;
			ParticleSystem trailSystem = trailParticleEmitter.GetComponent<ParticleSystem>();
			trailSystem.startColor = actualTargetColor;

			//Are we sliding on a wall? If so, give us an invincibility buffer if we don't have it already
			if(this.canInvincible){
				//We want to start an invincibility timer
				StartCoroutine(this.StartInvincibilityTimer());
			}
		}

	}

	//For jumping
	void Jump(bool boostJump = false)
	{
		//Launch it!

		//Which way?
		int jumpImpulse = 1;
		if (this.jumpLeft)
		{
			//Jump to the left!
			jumpImpulse = -1;
		}
		this.jumpLeft = !this.jumpLeft;

		//Get the jump amount
		float xImpulse = this.jumpImpulseX;
		float yImpulse = this.jumpImpulseY;
		int particleCount = this.jumpParticles;

		//Are we boosting?
		if (boostJump)
		{
			//Use boost values
			xImpulse = this.jumpImpulseXBoost;
			yImpulse = this.jumpImpulseYBoost;
			particleCount = this.boostParticles;
		}

		//Where to launch it?
		Vector2 force = new Vector2(xImpulse * jumpImpulse, yImpulse);
		ForceMode2D forceMode = ForceMode2D.Impulse;

		//Apply the force
		Rigidbody2D rigidBody = this.GetComponent<Rigidbody2D>();
		rigidBody.velocity = Vector2.zero;
		rigidBody.AddForce(force, forceMode);

		//We cannot jump till we hit a wall
		this.canJump = false;
		this.touchingWall = false;
		this.jumping = true;

		//Play the jump sound
		this.PlayJumpSound();

		//Make a slight particle burst
		ParticleSystem particleSystem = this.GetComponent<ParticleSystem>();
		particleSystem.Emit(particleCount);

		//We cannot jump again until we hit another wall
		this.canJump = false;

		//If we wanted to, we can prejump
		this.canPrejump = true;
		this.prejumping = false;

		//We can't go invincible while jumping
		this.canInvincible = false;

	}

	//For crashing
	void Crash()
	{
		//We want to stop moving
		//Change our material
		GetComponent<BoxCollider2D>().sharedMaterial = this.crashMaterial;


		//Which way?
		int jumpImpulse = 1;
		if (this.jumpLeft)
		{
			//Jump to the left!
			jumpImpulse = -1;
		}

		//Where to launch it?
		float xImpulse = Random.Range(this.crashImpulseXMin, this.crashImpulseXMax);
		float yImpulse = Random.Range(this.crashImpulseYMin, this.crashImpulseYMax);
		float zImpulse = Random.Range(this.crashImpulseZMin, this.crashImpulseZMax);

		Vector2 force = new Vector2(xImpulse * jumpImpulse, yImpulse);
		ForceMode2D forceMode = ForceMode2D.Impulse;

		//Apply the force
		Rigidbody2D rigidBody = this.GetComponent<Rigidbody2D>();
		rigidBody.velocity = Vector2.zero;
		rigidBody.AddForce(force, forceMode);
		rigidBody.AddTorque(zImpulse * jumpImpulse);

		//We're going to vibrate the handheld too
		bool shouldVibrate = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_VIBRATE);
		if (shouldVibrate)
		{
			//Call vibration. This is ignored if there is no vibration
#if UNITY_ANDROID || UNITY_IPHONE
			Handheld.Vibrate();
#endif
		}
		
	}

	//Handle the invincibility timer
	private IEnumerator StartInvincibilityTimer()
	{
		//Set the flag
		this.invincible = true;
		this.canInvincible = false;

		//Wait some time
		yield return new WaitForSeconds(this.invincibilityBufferTime);

		//Turn off the flag
		this.invincible = false;
	}

	//For playing the jump noise
	void PlayJumpSound()
	{
		//Play jump
		this.PlaySound(AudioController.Instance.jumpSFX);
	}

	//For dying the first time
	void Kill(int reason)
	{
		//Did we hit a wall?
		if(reason != GameController.DEATH_LASER)
		{
			//Play the crash sound
			this.PlayCrashSound();
		}
		
		this.dead = true;

		//And we want to act like we crashed
		this.Crash();

		//Tell the game controller we died
		this.gameController.GetComponent<GameController>().PlayerKilled(reason);
	}

	//For playing the death sound
	void PlayCrashSound()
	{
		//Play it
		this.PlaySound(AudioController.Instance.crashSFX);
	}

	//For playing the laser sound
	void PlayLaserSound()
	{
		//Play it
		this.PlaySound(AudioController.Instance.burnSFX);
	}

	//Playing sound effects in general
	void PlaySound(AudioClip audioClip)
	{
		//Are SFX enabled?
		if (SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_SFX))
		{
			//Get the audio source and play the sound
			AudioController.Instance.PlaySFX(audioClip);
		}	
	}

	//For not jumping as much
	void JumpLess()
	{
		//Gravity affects us more, but only when jumping
		if (this.jumping)
		{
			this.SetGravity(this.gravityMore);
		}
	}

	//For jumping more
	void JumpMore()
	{
		//Gravity affects us less
		this.SetGravity(this.gravity);
	}

	//Gravity will change...
	void SetGravity(float gravity)
	{
		//Set it!
		GetComponent<Rigidbody2D>().gravityScale = gravity;
	}

	//When a wall is hit
	void OnHitWallCollider(WallColliderController wallColliderController)
	{
		//Track that we are now contacting a wall
		this.touchingWall = true;

		//No longer jumping
		this.jumping = false;

		//We can jump
		this.canJump = true;

		//We can go invincible temporarily
		this.canInvincible = true;

		//Gravity goes back to normal
		this.JumpMore();

		//Were we prejumping?
		if (this.prejumping)
		{
			//We can now jump
			this.Jump(true);
		}
	}

	//When a tile is hit
	void OnHitWallTile(WallTileController wallTileController)
	{
		//What color did we hit?
		//Was it bad?
		if (wallTileController.color != ColorHelper.COLORS_ENUM.SAFE && !wallTileController.color.Equals(this.currentColor))
		{
			//We die if we aren't invincible

			//But only die once
			if (!this.dead && !this.invincible)
			{
				//Kill it

				//Why did we fall?
				int reason = GameController.DEATH_COLOR;
				if (wallTileController.color == ColorHelper.COLORS_ENUM.DANGEROUS)
					reason = GameController.DEATH_BLACK;
				this.Kill(reason);
			}
		}
	}

	//We hit the laser
	void OnHitLaser(LaserController laser)
	{
		//Is the laser even on?
		if (laser.laserOn)
		{
			//Are we not dead?
			if (!this.dead)
			{
				//Ok, we die to the laser
				this.Kill(GameController.DEATH_LASER);
			}

			//Have we burned already?
			if (!this.burned)
			{
				//Time to burn up!
				this.burned = true;

				//We need to play the sound
				this.PlayLaserSound();

				//Create a few exploding bits
				for (int i = 0; i < this.playerBitCount; ++i)
				{
					//Make one
					PlayerBitController playerBitController = ((GameObject)Instantiate(this.playerBitPrefab, this.transform.position, new Quaternion())).GetComponent<PlayerBitController>();

					//Set the color
					bool colorBlind = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_COLORBLIND);
					playerBitController.color = ColorHelper.GetColorFromEnum(this.currentColor, colorblind: colorBlind);
				}

				//And go away
				this.gameObject.GetComponent<SpriteRenderer>().enabled = false;

				//Hide the trail emitter
				GameObject trailParticleEmitter = this.transform.Find("ParticleTrail").gameObject;
				ParticleSystem trailSystem = trailParticleEmitter.GetComponent<ParticleSystem>();
				trailSystem.Stop();
			}

			
		}
	}

	//We hit an obstacle
	void OnHitObstacle(ObstacleController obstacle)
	{
		//Are we already dead? Did the color not match?
		if (!this.dead && !obstacle.color.Equals(this.currentColor))
		{
			//We crash
			this.Kill(GameController.DEATH_COLOR);
		}
	}

	//When we hit something
	void OnCollisionEnter2D(Collision2D collision)
	{
		//Did we hit a wall?
		if (collision.gameObject.name == "WallCollider")
		{
			//Handle hitting the wall
			this.OnHitWallCollider(collision.gameObject.GetComponent<WallColliderController>());
		}
	}

	//When colliding with a trigger
	void OnTriggerEnter2D(Collider2D collision)
	{
		//Did we hit a wall tile?
		if (collision.gameObject.name == "WallTile" || collision.gameObject.name == "WallTile(Clone)")
		{
			//We want to handle contacting a tile
			this.OnHitWallTile(collision.gameObject.GetComponent<WallTileController>());
		}
		else if (collision.gameObject.name == "Laser")
		{
			//Handle hitting the laser
			this.OnHitLaser(collision.gameObject.GetComponent<LaserController>());
		}

		//Did we hit an obstacle?
		if (collision.gameObject.GetComponent<ObstacleController>() != null)
		{
			//We need to see if we die
			this.OnHitObstacle(collision.gameObject.GetComponent<ObstacleController>());
		}
	}
}
