﻿using UnityEngine;
using System.Collections;

public class PlayerBitController : MonoBehaviour {

	//The color
	public Color color;

	//For ignoring the laser
	private float ignoreLaserTime = 0.3f;
	private bool ignoreLaser = true;

	//Physics variables
	private float impulseXMin = -5;
	private float impulseXMax = 10;
	private float impulseYMin = 10;
	private float impulseYMax = 17;
	private float impulseZMin = -15;
	private float impulseZMax = 15;
	


	// Use this for initialization
	void Start () {

		//Match the color
		this.gameObject.GetComponent<SpriteRenderer>().color = this.color;

		//Set a timer
		StartCoroutine(this.IgnoreLaser());

		//Where to launch?
		float xImpulse = Random.Range(this.impulseXMin, this.impulseXMax);
		float yImpulse = Random.Range(this.impulseYMin, this.impulseYMax);
		float zImpulse = Random.Range(this.impulseZMin, this.impulseZMax);

		Vector2 force = new Vector2(xImpulse, yImpulse);
		ForceMode2D forceMode = ForceMode2D.Impulse;

		//Apply the force
		Rigidbody2D rigidBody = this.GetComponent<Rigidbody2D>();
		rigidBody.velocity = Vector2.zero;
		rigidBody.AddForce(force, forceMode);
		rigidBody.AddTorque(zImpulse);
	}


	IEnumerator IgnoreLaser()
	{
		//We want to wait..
		yield return new WaitForSeconds(this.ignoreLaserTime);

		//No longer ignoring
		this.ignoreLaser = false;
	}

	//We hit the laser
	void OnHitLaser(LaserController laser)
	{
		//We want fizz out, so play a smokey particle

		//And then we want to play a sound
		//Get the audio source and play the sound
		AudioController.Instance.PlaySFX(AudioController.Instance.fizzSFX);

		//And we want to stop showing our sprite
		this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
	}

	// Update is called once per frame
	void Update () {
	
	}


	//When colliding with a trigger
	void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.name == "Laser")
		{
			//Handle hitting the laser
			if (!this.ignoreLaser)
				this.OnHitLaser(collision.gameObject.GetComponent<LaserController>());
		}
	}
}
