﻿using UnityEngine;
using SimpleJSON;
using System.IO;
using System.Text;
using System.Collections;

public class LevelBuilderController : MonoBehaviour {

	//Prefabs
	public GameObject wallPrefab;
	private float wallPrefabHeight;

	//The player
	private GameController gameController;
	private PlayerController player;

	//For tracking the RNG
	private RandomHelper randomNumberGenerator;

	//How many tiles tall is the intro?
	private int introHeight = 20;

	//What height are we at currently?
	private float currentLeftHeight = 0f;
	private float currentRightHeight = 0f;

	//Tracking previous wall tiles
	private WallTileController previousLeftWallTileController;
	private WallTileController previousRightWallTileController;

	//All the patterns
	public TextAsset jsonText;
	private JSONNode patternsJSON;

	//Where are we pattern wise on either side? What about color?
	private string leftPattern;
	private string rightPattern;
	private ColorHelper.COLORS_ENUM leftColor;
	private ColorHelper.COLORS_ENUM rightColor;
	private int leftColorDuration;
	private int rightColorDuration;

	//Obstacles
	public GameObject obstacle1x1;
	public GameObject obstacle2x2;
	public GameObject obstacle2x1;
	public GameObject obstacle1x2;
	public GameObject obstacle3x1;
	public GameObject obstacle1x3;
	private int obstacleWait = 30;

	// Use this for initialization
	void Start () {
		//Lets get the player
		this.player = ToolboxSingleton.Instance.FindPlayerController();
		this.gameController = ToolboxSingleton.Instance.FindGameController();

		//Get the random number generator
		this.randomNumberGenerator = new RandomHelper(this.gameController.seed);

		//Get all the patterns
		this.GetPatterns();

		//The default pattern is always a bunch of white ones with some obstacle
		string leftHole = "11";
		string rightHole = "10";
		if(this.randomNumberGenerator.NextFloat(0f, 1f) > 0.5f)
		{
			leftHole = "10";
			rightHole = "11";
		}
		this.leftPattern = new string('0', this.introHeight) + leftHole;
        this.rightPattern = new string('0', this.introHeight) + rightHole;
		this.leftColor = ColorHelper.COLORS_ENUM.SAFE;
		this.rightColor = ColorHelper.COLORS_ENUM.SAFE;
		this.leftColorDuration = this.introHeight;
		this.rightColorDuration = this.introHeight;

		//Get the height
		this.wallPrefabHeight = 1f;//this.wallPrefab.GetComponent<SpriteRenderer>().sprite.bounds.size.y;
		
		//Start up halfway so we are centered
		this.currentLeftHeight += this.wallPrefabHeight / 2;
		this.currentRightHeight += this.wallPrefabHeight / 2;

		//Normalize the score
		this.gameController.IncrementScore(-(this.introHeight * 2)); //40 draw on start
	}

	//Get all the pattern info from the json
	void GetPatterns()
	{
		//Parse the json
		this.patternsJSON = JSON.Parse(this.jsonText.text);
	}

	//Get a random color
	ColorHelper.COLORS_ENUM GetRandomColor(bool includeSafe = false, bool includeDangerous = false)
	{
		//Simple. Put all the colors into a list...
		//Pick one randomly from the remaining colors
		ArrayList colors = new ArrayList();

		//Start adding
		colors.Add(ColorHelper.COLORS_ENUM.COLOR1);
		colors.Add(ColorHelper.COLORS_ENUM.COLOR2);
		colors.Add(ColorHelper.COLORS_ENUM.COLOR3);

		//Do we add white or black?
		if (includeSafe)
			colors.Add(ColorHelper.COLORS_ENUM.SAFE);
		if (includeDangerous)
			colors.Add(ColorHelper.COLORS_ENUM.DANGEROUS);

		//Pick one randomly
		int colorIndex = this.randomNumberGenerator.Next(0, colors.Count);
		return (ColorHelper.COLORS_ENUM)colors[colorIndex];
	}

	//Pick a new color
	ColorHelper.COLORS_ENUM PickNewColor()
	{
		//Start getting the next color
		ColorHelper.COLORS_ENUM newColor;

		//Based on difficulty, how likely are we to get white?
		int difficulty = this.GetDifficultyIndex();

		//Well, lets see then
		float safeChance = (GameController.MAX_DIFFICULTY - (difficulty) / GameController.MAX_DIFFICULTY);
		safeChance = this.GetPatternMap()["safe"].AsFloat;

		float safeRoll = this.randomNumberGenerator.NextFloat(0f, 1f);

		//Now get a random value and see
		if (safeRoll < safeChance)
		{
			//Use white!
			newColor = ColorHelper.COLORS_ENUM.SAFE;
		}
		else
		{
			//Pick one randomly from the remaining colors
			newColor = this.GetRandomColor();
        }

		//We've got a color. Give it back
		return newColor;
	}

	//Get the difficulty
	int GetDifficultyIndex()
	{
		//Get all the difficulties
		JSONArray difficultyList = this.patternsJSON["difficultyMap"].AsArray;

		//Which difficulty do we best match?
		int difficultyLevel = -1;
		foreach(JSONNode difficultyNode in difficultyList)
		{
			//Get the difficulty
			int difficultyHeight = difficultyNode["distance"].AsInt;
			if((this.currentLeftHeight + this.currentRightHeight) > difficultyHeight * 2)
			{
				//We are at least this difficulty
				difficultyLevel += 1;
			}
			else
			{
				//We aren't at this difficulty. Break
				break;
			}
		}
		this.gameController.difficulty = difficultyLevel;
		return difficultyLevel;
	}

	//Get the difficulty
	string GetDifficultyName(int difficultyIndex)
	{
		//Get all the difficulties
		JSONArray difficultyList = this.patternsJSON["difficultyMap"].AsArray;

		//Get the difficulty information
		JSONNode difficultyNode = difficultyList[difficultyIndex];

		//Return the name
		return difficultyNode["name"];
	}

	JSONNode GetPatternMap(string difficultyName = null)
	{
		//Do they want a particular string?
		if(difficultyName == null)
		{
			//We want to get it
			difficultyName = this.GetDifficultyName(this.GetDifficultyIndex());
        }

		//Get the pattern map
		return this.patternsJSON[difficultyName];
	}

	//Do we need to place an obstacle?
	Vector2 GetObstacle()
	{
		//How long to have the next obstacle?
		Vector2 size;

		//Figure it out
		//Get it from the level that matches the current difficulty
		JSONNode patternMap = this.GetPatternMap();
		JSONNode obstacleMap = patternMap["obstacle"];
		JSONArray obstacleArray = obstacleMap["obstacles"].AsArray;

		//We might not even do anything...
		float frequency = obstacleMap["frequency"].AsFloat;
		int waitTime = obstacleMap["delay"].AsInt;
		float obstacleRoll = this.randomNumberGenerator.NextFloat(0f, 1f);
		bool makeObstacle = obstacleRoll < frequency;

		//Did we want an obstacle?
		if (makeObstacle)
		{
			//From there, get all the chances
			int sum = 0;
			ArrayList patterns = new ArrayList();
			foreach (JSONNode obstacleNode in obstacleArray)
			{
				//Get the info from it
				int chance = obstacleNode["chance"].AsInt;
				int width = obstacleNode["width"].AsInt;
				int height = obstacleNode["height"].AsInt;
				Vector2 obstacleSize = new Vector2(width, height);

				//Calcualte chances
				sum += chance;
				for (int i = 0; i < chance; ++i)
				{
					patterns.Add(obstacleSize);
				}
			}

			//Now roll the dice
			int random = this.randomNumberGenerator.Next(0, sum);

			//Get it
			size = (Vector2)patterns[random];
			this.obstacleWait = waitTime;
		}
		else
		{
			//Don't actually make anything this time then
			this.obstacleWait = 0;
			size = new Vector2(0, 0);
		}

		//Return the size we got
		return size;
	}

	//Do we need to change colors?
	int GetColorSwap()
	{
		//How much to swap?
		int swapSize = 0;

		//Figure it out
		//Get it from the level that matches the current difficulty
		JSONNode patternMap = this.GetPatternMap();
		JSONNode swapMap = patternMap["swap"];
		JSONArray swapArray = swapMap["patterns"].AsArray;

		//We might not even do anything...
		float frequency = swapMap["frequency"].AsFloat;
		if (this.randomNumberGenerator.NextFloat(0f, 1f) < frequency)
		{
			//From there, get all the chances
			int sum = 0;
			ArrayList patterns = new ArrayList();
			foreach (JSONNode patternNode in swapArray)
			{
				//Get the info from it
				int chance = patternNode["chance"].AsInt;
				int size = patternNode["size"].AsInt;

				//Calcualte chances
				sum += chance;
				for (int i = 0; i < chance; ++i)
				{
					patterns.Add(size);
				}
			}

			//Now roll the dice
			int random = this.randomNumberGenerator.Next(0, sum);

			//Get it
			swapSize = swapSize + (int)patterns[random];
		}
		else
		{
			//Don't actually make anything this time then
			swapSize += -1;
		}

		//Return the size
		return swapSize;
	}

	//Get the next appropriate pattern
	string GetHolePattern()
	{
		//Get a string ready
		string holePattern = "";

		//Figure it out
		//Get it from the level that matches the current difficulty
		JSONNode patternMap = this.GetPatternMap();
		JSONNode holeMap = patternMap["hole"];
		JSONArray patternArray = holeMap["patterns"].AsArray;

		//We might not even do anything...
		float frequency = holeMap["frequency"].AsFloat;
		if(this.randomNumberGenerator.NextFloat(0f, 1f) < frequency)
		{
			//From there, get all the chances
			int sum = 0;
			ArrayList patterns = new ArrayList();
			foreach (JSONNode patternNode in patternArray)
			{
				//Get the info from it
				int chance = patternNode["chance"].AsInt;
				string pattern = patternNode["pattern"];

				//Calcualte chances
				sum += chance;
				for (int i = 0; i < chance; ++i)
				{
					patterns.Add(pattern);
				}
			}

			//Now roll the dice
			int random = this.randomNumberGenerator.Next(0, sum);

			//Get it
			holePattern += patterns[random];
		}
		else
		{
			//Don't actually make anything this time then
			holePattern += '0'; 
		}

		//Return
		return holePattern;
	}

	//Build the obstacle
	void BuildObstacle(Vector2 obstacleSize, float currentY)
	{
		//Is there a real obstacle?
		int x = (int)obstacleSize.x;
		int y = (int)obstacleSize.y;
		bool validObstacle = (x != 0 && y != 0);
		if (validObstacle)
		{
			//Figure out what to make
			GameObject obstaclePrefab;

			//What do we have?
			if(x == 2 && y == 2)
			{
				//Use the big one
				obstaclePrefab = this.obstacle2x2;
			}else if(x == 2 && y == 1)
			{
				//Use the long one
				obstaclePrefab = this.obstacle2x1;
			}else if(x == 1 && y == 2)
			{
				//Use the tall one
				obstaclePrefab = this.obstacle1x2;
			}
			else if (x == 3 && y == 1)
			{
				//Use the long one
				obstaclePrefab = this.obstacle3x1;
			}
			else if (x == 1 && y == 3)
			{
				//Use the tall one
				obstaclePrefab = this.obstacle1x3;
			}
			else
			{
				//Default to the small one
				obstaclePrefab = this.obstacle1x1;
			}

			//Figure out where to place it
			float obstacleRegion = ToolboxSingleton.Instance.OBSTACLE_REGION_WIDTH / 2;
            float newX = this.randomNumberGenerator.NextFloat(-obstacleRegion, obstacleRegion);
			Vector3 position = new Vector3(newX, currentY, 0);

			//Get a color at random
			ColorHelper.COLORS_ENUM obstacleColor = this.GetRandomColor();

			//Make one
			ObstacleController obstacleController = ((GameObject)Instantiate(obstaclePrefab, position, new Quaternion())).GetComponent<ObstacleController>();

			//Set the color
			obstacleController.color = obstacleColor;
		}
	}

	//Build a wall on the specified side
	void BuildWall(bool leftSide)
	{
		//Which pattern?
		string currentPattern = this.rightPattern;
		ColorHelper.COLORS_ENUM currentColor = this.rightColor;
		int currentColorDuration = this.rightColorDuration;

		//Left side?
		if (leftSide)
		{
			//Use left variables
			currentPattern = this.leftPattern;
			currentColor = this.leftColor;
			currentColorDuration = this.leftColorDuration;
		}

		//Do we need to change colors?
		if(currentColorDuration < 1)
		{
			//Yes we do
			currentColorDuration = this.GetColorSwap();

			//Pick a new color?
			if(currentColorDuration != -1)
			{
				//Pick a color
				currentColor = this.PickNewColor();
			}
		}

		//Do we want to put a new obstacle?
		if (this.obstacleWait < 1)
		{
			//Try to make a new one
			Vector2 obstacleSize = this.GetObstacle();

			//Make it, if applicable
			this.BuildObstacle(obstacleSize, Mathf.Min(this.currentLeftHeight, this.currentRightHeight));
		}
		
		//Always decrement the wait time
		--this.obstacleWait;

		//Do we have a current pattern?
		if (currentPattern.Length > 0)
		{
			//We can pop off a character
			char nextTile = currentPattern[0];
			currentPattern = currentPattern.Substring(1);

			//Are we adding a black tile?
			if (nextTile == '1')
			{
				//Yes
				this.PlaceWall(ColorHelper.COLORS_ENUM.DANGEROUS, leftSide);
			}
			else
			{
				//We place it of the current active color for that side
				this.PlaceWall(currentColor, leftSide);
			}

			//Either way we need to do one less color
			currentColorDuration -= 1;

			//Also the user gets a point for placing a wall
			this.gameController.IncrementScore();
		}
		else
		{
			//We don't have a pattern for this side. We want to generate a new one
			string newPattern = this.GetHolePattern();
			currentPattern += newPattern;
		}

		//Set it back...
		if (leftSide)
		{
			//Set the left pattern
			this.leftPattern = currentPattern;
			this.leftColor = currentColor;
			this.leftColorDuration = currentColorDuration;
		}
		else
		{
			//Set the right pattern
			this.rightPattern = currentPattern;
			this.rightColor = currentColor;
			this.rightColorDuration = currentColorDuration;
		}
	}
	
	//Actually place a wall down
	void PlaceWall(ColorHelper.COLORS_ENUM color, bool leftSide)
	{
		//Which side?
		float targetX = ToolboxSingleton.Instance.TOWER_WIDTH / 2;
		float targetY = this.currentRightHeight;
		if (leftSide)
		{
			//Put it there!
			targetX = -targetX;
			targetY = this.currentLeftHeight;
		}

		//Now place it
		GameObject wallTile = (GameObject)Instantiate(this.wallPrefab, new Vector3(targetX, targetY, 0), new Quaternion());

		//Set its color
		WallTileController wallTileController = wallTile.GetComponent<WallTileController>();
		wallTileController.color = color;

		//Increase the height and track the previous
		if (leftSide)
		{
			this.currentLeftHeight += this.wallPrefabHeight;

			//Do we have one?
			if (this.previousLeftWallTileController)
			{
				//We want to tell it to look at us
				this.previousLeftWallTileController.nextWallTileController = wallTileController;
			}
			//Point to the next one
			this.previousLeftWallTileController = wallTileController;
		}
		else
		{
			this.currentRightHeight += this.wallPrefabHeight;

			//Do we have one?
			if (this.previousRightWallTileController)
			{
				//We want to tell it to look at us
				this.previousRightWallTileController.nextWallTileController = wallTileController;
			}
			//Point to the next one
			this.previousRightWallTileController = wallTileController;
		}

	}

	// Update is called once per frame
	void Update () {

		//Are we within some heights of the topmost tile?
		float playerY = this.player.transform.position.y;

		//What is the height of one of them?


		//Too close?
		while (playerY > this.currentLeftHeight - wallPrefabHeight * this.introHeight)
		{
			//We need to add tiles
			this.BuildWall(true);
		}
		while (playerY > this.currentRightHeight - wallPrefabHeight * this.introHeight)
		{
			//We need to add tiles
			this.BuildWall(false);
		}
	}
}
