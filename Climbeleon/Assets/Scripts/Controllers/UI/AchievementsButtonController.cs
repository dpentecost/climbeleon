﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class AchievementsButtonController : MonoBehaviour {

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update () {

		//Is social ready?
		bool canAchievements = SocialSingleton.Instance.SocialIsReady() || SocialSingleton.Instance.CanSocial();
		if (canAchievements)
		{
			//We want to be a usable button
			Button achievementsButton = this.gameObject.GetComponent<Button>();
			achievementsButton.interactable = canAchievements;
		}
	}

	//When we want to view achievements
	public void ViewAchievements()
	{
		//Are we ready to do the achievements?
		if (SocialSingleton.Instance.SocialIsReady())
		{
			SocialSingleton.Instance.ShowAchievements();
		}
		else if (SocialSingleton.Instance.CanSocial())
		{
			//We can't show the achievements but maybe we can log in instead
			SocialSingleton.Instance.AuthenticateUser();
		}
	}
}
