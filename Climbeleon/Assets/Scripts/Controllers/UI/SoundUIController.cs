﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SoundUIController : MonoBehaviour {

	//The GUI skin
	public GUISkin customGUISkin;

	//Sprites that the controller uses

	//Volume buttons
	public Texture textureSoundOn;
	public Texture textureSoundOff;
	public Texture textureMusicOn;
	public Texture textureMusicOff;

	//Some UI variables IN PIXELS
	private float buttonMarginEdge = 10;
	//private float buttonPaddingVertical = 10;
	private float buttonPaddingHorizontal = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// For the GUI
	void OnGUI()
	{
		//Use the skin
		GUI.skin = this.customGUISkin;

		//Make the sound toggle button
		Texture soundTexture = this.GetSoundTexture();
		float soundTextureHeight = soundTexture.height;
		float soundTextureWidth = soundTexture.width;
		if (GUI.Button(new Rect(this.buttonMarginEdge, Screen.height - soundTextureHeight - this.buttonMarginEdge, soundTextureWidth, soundTextureHeight), soundTexture))
		{
			//User wants to toggle SFX
			AudioController.Instance.ToggleSFX();
		}

		//Now do the music toggle
		Texture bgmTexture = this.GetBackgroundMusicTexture();
		float bgmTextureHeight = bgmTexture.height;
		float bgmTextureWidth = bgmTexture.width;
		if (GUI.Button(new Rect(this.buttonMarginEdge + soundTextureWidth + this.buttonPaddingHorizontal, Screen.height - bgmTextureHeight - this.buttonMarginEdge, bgmTextureWidth, bgmTextureHeight), bgmTexture))
		{
			//We want to toggle BGM
			AudioController.Instance.ToggleBGM();
		}
	}

	//For getting the correct texture for the SFX button
	private Texture GetSoundTexture()
	{
		//Are we playing SFX?
		if (SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_SFX))
		{
			//Use the play texture
			return this.textureSoundOn;
		}
		else
		{
			return this.textureSoundOff;
        }
	}

	//For getting the correct texture for the BGM button
	private Texture GetBackgroundMusicTexture()
	{
		//Are we playing SFX?
		if (SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_BGM))
		{
			//Use the play texture
			return this.textureMusicOn;
		}
		else
		{
			return this.textureMusicOff;
		}
	}
}
