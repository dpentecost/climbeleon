﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

abstract public class ToggleButtonController : MonoBehaviour {

	//Each of these buttons has 2 icons
	public Sprite spriteOn;
	public Sprite spriteOff;

	//And a current state
	private Sprite currentSprite = null;

	//What to do when pressed

	// Use this for initialization
	public virtual void Start () {

		//Use our delegate. We better have a button
		this.gameObject.GetComponent<Button>().onClick.AddListener(
			delegate
			{
				this.OnToggle();
			});
	}
	
	// Update is called once per frame
	public virtual void Update () {

		//Always get the most up-to-date sprite
		Sprite targetSprite = this.GetCurrentSprite();

		//Was it the same as the current one?
		if(targetSprite != this.currentSprite)
		{
			//Change it
			this.currentSprite = targetSprite;

			//And set the image accordingly
			this.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = this.currentSprite;
		}
	}

	// They need a way to get the current sprite
	abstract public Sprite GetCurrentSprite();

	//They need to do something when toggled
	abstract public void OnToggle();
}
