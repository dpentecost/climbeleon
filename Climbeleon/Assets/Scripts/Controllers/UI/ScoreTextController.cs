﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreTextController : MonoBehaviour {

	//The GameController to get the score from
	private GameController gameController;

	// Use this for initialization
	void Start () {
		//Get the GameController
		this.gameController = ToolboxSingleton.Instance.FindGameController();
	}
	
	// Update is called once per frame
	void Update () {

		//Get the score
		int score = gameController.GetScore();

		//Set our text
		this.GetComponent<Text>().text = score.ToString();
	}
}
