﻿using UnityEngine;
using System.Collections;

public class GameOverGUIController : MonoBehaviour {

	//The panel
	GameObject gameOverPanel;

	//The options panel
	public GameObject optionsGUI;

	// Use this for initialization
	void Start () {

		//Get the panel
		this.gameOverPanel = this.transform.Find("GameOverPanel").gameObject;

		//Hide
		this.HideGameOverPanel();

	}

	// When we push the back button
	public void HideGameOverPanel()
	{
		//We want to fly away and then disable
		float time = 0;
		float targetY = Screen.height * 2.5f;
		System.Action onComplete = this.Deactivate;

		//Now tween it
		LeanTween.moveLocalY(this.gameOverPanel, targetY, time).setEase(LeanTweenType.easeOutExpo).setOnComplete(onComplete);
	}

	//When we want it to appear
	public void ShowGameOverPanel()
	{
		//We want to enable and then fly in
		float targetY = 0;
		float time = 1;
		System.Action onStart = this.Activate;

		//Now tween it
		LeanTween.moveLocalY(this.gameOverPanel, targetY, time).setEase(LeanTweenType.easeOutExpo).setOnStart(onStart);
	}

	// EZ Deactivate
	public void Deactivate()
	{
		//Simple
		this.gameObject.SetActive(false);
	}
	// EZ Activate
	public void Activate()
	{
		//Simple
		this.gameObject.SetActive(true);
	}

	// Update is called once per frame
	void Update () {

		//Did the user press escape or back?
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			//They have pressed escape. 
			//We want to quit if we aren't showing options
			if (optionsGUI == null || !optionsGUI.activeInHierarchy)
			{
				//They aren't trying to exit the options. We'll quit
				ToolboxSingleton.Instance.FadeToScene(ToolboxSingleton.SCENE_MAIN);
			}
		}

	}
}
