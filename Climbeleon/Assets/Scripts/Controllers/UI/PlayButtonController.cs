﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayButtonController : MonoBehaviour {

	// Use this for initialization
	void Start () {

		//Use the safe color
		Color targetColor = ColorHelper.color1;

		//Get the button
		Button playButton = this.GetComponent<Button>();

		//Get the color block
		ColorBlock transitionColors = playButton.colors;

		//Get a corrected color block
		transitionColors = ColorHelper.GenerateColorBlock(transitionColors, targetColor);

		//Set it back
		playButton.colors = transitionColors;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
