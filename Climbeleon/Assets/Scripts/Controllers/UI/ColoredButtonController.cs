﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ColoredButtonController : MonoBehaviour {

	//Which color?
	public ColorHelper.COLORS_ENUM colorChoice;

	// Use this for initialization
	void Start()
	{
		
	}

	public void SetTransitionColors(Color targetColor)
	{
		//Get the button
		Button button = this.GetComponent<Button>();

		//Get the color block
		ColorBlock transitionColors = button.colors;

		//Get a corrected color block
		transitionColors = ColorHelper.GenerateColorBlock(transitionColors, targetColor);

		//Set it back
		button.colors = transitionColors;
	}

	// Update is called once per frame
	void Update()
	{
		//Set our own colors
		bool colorBlind = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_COLORBLIND);
		Color targetColor = ColorHelper.GetColorFromEnum(colorChoice, colorblind: colorBlind);

		//Now use that color
		this.SetTransitionColors(targetColor);
	}
}
