﻿using UnityEngine;
using System.Collections;

public class OptionsPanelController : MonoBehaviour {

	//The GUI panel
	public GameObject optionsPanel;

	//Is it ready?
	public bool ready = true;

	//The panel width
	//TODO: CALCULATE THIS??
	float panelWidth = 700;

	// Use this for initialization
	void Start () {

		//Get the panel
		this.optionsPanel = this.transform.Find("OptionsPanel").gameObject;

		//Start disabled
		this.HideOptionsPanel(quick: true);
	}
	
	// Update is called once per frame
	void Update () {

		//Is the user trying to exit the options panel?
		//Did the user press escape or back?
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			//They have pressed escape. 
			//Hide!
			this.HideOptionsPanel();
		}

	}

	// When we push the back button
	public void HideOptionsPanel(bool quick = false)
	{
		//Are we even ready?
		if (!this.ready)
		{
			//Nope
			return;
		}
		this.ready = false;

		//We want to fly away and then disable
		float targetX = -(Screen.width + panelWidth * 2);
		float time = 1;

		//Or are we doing it quickly?
		if (quick)
		{
			//Yep
			time = 0;
		}

		//What to do?
		System.Action onComplete = this.Deactivate;

		//Now tween it
		LeanTween.moveLocalX(this.optionsPanel, targetX, time).setEase(LeanTweenType.easeOutExpo).setOnComplete(onComplete);
	}

	//When we want it to appear
	public void ShowOptionsPanel()
	{
		//Are we even ready?
		if (!this.ready)
		{
			//Nope
			return;
		}
		this.ready = false;

		//We want to enable and then fly in
		float targetX = 0;
		float time = 1;

		//Get the action
		System.Action<object> onComplete = this.SetReady;
		Hashtable parameters = new Hashtable();
		parameters.Add("onCompleteTarget", this.gameObject);

		//Now tween it
		this.Activate();
		LeanTween.moveLocalX(this.optionsPanel, targetX, time).setEase(LeanTweenType.easeOutExpo).setOnComplete(onComplete, parameters);
	}

	

	// EZ Deactivate
	public void Deactivate()
	{
		//Simple
		this.SetReady(null);
		this.gameObject.SetActive(false);
	}

	//For tracking status
	public void SetReady(object parameter)
	{
		//Set the flag
		this.ready = true;
	}

	// EZ Activate
	public void Activate()
	{
		//Simple
		this.gameObject.SetActive(true);
	}
}
