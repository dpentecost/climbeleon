﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AttributionController : MonoBehaviour {

	// Use this for initialization
	void Start () {

		//Set the correct bgm
		AudioController.Instance.LoopBGM(AudioController.Instance.menuBGM);

		//Back button goes to the main menu scene
		GameObject backButton = GameObject.Find("BackButton");
		backButton.GetComponent<Button>().onClick.AddListener(delegate { ToolboxSingleton.Instance.FadeToScene(ToolboxSingleton.SCENE_MAIN); });

	}
	
	// Update is called once per frame
	void Update () {

		//Did the user press escape or back?
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			//They have pressed escape. 
			//Go back to the main menu
			ToolboxSingleton.Instance.FadeToScene(ToolboxSingleton.SCENE_MAIN);
		}

	}
}
