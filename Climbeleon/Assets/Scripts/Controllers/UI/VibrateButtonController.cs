﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VibrateButtonController : ToggleButtonController {

	public override void Start()
	{
		//Disable if we are on standalone
#if UNITY_EDITOR || UNITY_STANDALONE
		Button vibrateButton = this.gameObject.GetComponent<Button>();
		vibrateButton.interactable = false;
#endif
	}

	public override Sprite GetCurrentSprite()
	{
		//Are we Vibrating?
		if (SettingsAndControls.IsLoaded())
		{
			if (SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_VIBRATE))
			{
				//Use the on texture
				return this.spriteOn;
			}
			else
			{
				//Nope
				return this.spriteOff;
			}
		}
		else
		{
			//Default to off
			return this.spriteOff;
		}
	}

	public override void OnToggle()
	{
		//Toggle the Colorblind
		bool vibrating = ToolboxSingleton.Instance.ToggleVibration();

		//Are we gonna vibrate?
		if (vibrating)
		{
			//We want to vibrate why not
#if UNITY_ANDROID || UNITY_IPHONE
			Handheld.Vibrate();
#endif
		}
	}
}
