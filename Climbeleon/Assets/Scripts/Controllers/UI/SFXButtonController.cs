﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SFXButtonController : ToggleButtonController {

	public override Sprite GetCurrentSprite()
	{
		//Are we playing SFX?
		if (SettingsAndControls.IsLoaded())
		{
			if (SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_SFX))
			{
				//Use the play texture
				return this.spriteOn;
			}
			else
			{
				//We aren't playing
				return this.spriteOff;
			}
		}
		else
		{
			//Default to off
			return this.spriteOff;
		}
	}

	public override void OnToggle()
	{
		//Toggle the SFX
		AudioController.Instance.ToggleSFX();
	}
}
