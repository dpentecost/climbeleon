﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

	// The Options Panel
	public GameObject optionsGUI;


	// Use this for initialization
	void Start () {

		//Set the correct bgm
		AudioController.Instance.LoopBGM(AudioController.Instance.menuBGM);

		//Set the colors of menu items. Also set their actions

		//Play button goes to the main scene
		GameObject playButton = GameObject.Find("PlayButton");
		playButton.GetComponent<Button>().onClick.AddListener(delegate { ToolboxSingleton.Instance.FadeToScene(ToolboxSingleton.SCENE_GAME); });

		//Attribution button goes to the attribution scene
		GameObject attributionButton = GameObject.Find("AttributionButton");
		attributionButton.GetComponent<Button>().onClick.AddListener(delegate { ToolboxSingleton.Instance.FadeToScene(ToolboxSingleton.SCENE_ATTRIBUTION); });

		//Quit button uf we are on standalone
		GameObject quitButton = GameObject.Find("QuitGameButton");
		quitButton.GetComponent<Button>().onClick.AddListener(delegate { ToolboxSingleton.Instance.QuitGame(); });

		//Are we on a platform that can't quit?
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_WEBGL
		//Kill the quit
		Destroy(GameObject.Find("QuitButton"));
#endif

	}

	// Update is called once per frame
	void Update () {

		//Did the user press escape or back?
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			//They have pressed escape. 
			//We want to quit if we aren't showing options
			if (optionsGUI == null || !optionsGUI.activeInHierarchy)
			{
				//They aren't trying to exit the options. We'll quit
				ToolboxSingleton.Instance.QuitGame();
			}
		}
	}
}
