﻿using UnityEngine;
using System.Collections;

public class GameUIController : MonoBehaviour {

	//The general GUI goodies
	public GameObject gameOverGUI;
	public GameObject scoreGUI;

	//The Portrait and Landscape GUIs
	public GameObject mobilePortraitGUI;
	public GameObject mobileLandscapeGUI;


	// Use this for initialization
	void Start () {

		//Don't show the Controls GUI unless we are on mobile
#if UNITY_STANDALONE
		mobilePortraitGUI.SetActive(false);
		mobileLandscapeGUI.SetActive(false);
#endif
	}

	public void ShowGameOverPanel(bool hide = false)
	{
		//Get the GameOver controller
		GameOverGUIController gameOver = gameOverGUI.GetComponent<GameOverGUIController>();

		//Hide or show
		if (hide)
		{
			//Hide
			gameOver.HideGameOverPanel();
		}
		else
		{
			//We show
			gameOver.ShowGameOverPanel();
		}
	}

	// Update is called once per frame
	void Update () {

		//Get the orientation if we are on mobile
#if UNITY_ANDROID || UNITY_IPHONE
		ScreenOrientation orientation = Screen.orientation;

		//Is it portrait?
		if (orientation == ScreenOrientation.Portrait || orientation == ScreenOrientation.PortraitUpsideDown)
		{
			//We need to show the portrait GUI, but only if we are on mobile
			mobilePortraitGUI.SetActive(true);
			mobileLandscapeGUI.SetActive(false);
		}
		else if (orientation == ScreenOrientation.LandscapeLeft || orientation == ScreenOrientation.LandscapeRight)
		{
			//Show the landscape gui
			mobileLandscapeGUI.SetActive(true);
			mobilePortraitGUI.SetActive(false);
		}
#endif

		//Do we just not show any gui?
		if (ToolboxSingleton.NO_GUI)
		{
			//Just show nothing
			mobilePortraitGUI.SetActive(false);
			mobilePortraitGUI.SetActive(false);
			gameOverGUI.SetActive(false);
			scoreGUI.SetActive(false);
		}

	}
}
