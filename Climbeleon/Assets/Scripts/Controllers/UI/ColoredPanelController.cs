﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColoredPanelController : MonoBehaviour {

	//Which color?
	public ColorHelper.COLORS_ENUM colorChoice;

	// Use this for initialization
	void Start()
	{
		//Set our own colors
		Color targetColor = ColorHelper.GetColorFromEnum(colorChoice);

		//Now use that color
		this.SetPanelColor(targetColor);
	}

	//For setting the panel's color
	public void SetPanelColor(Color targetColor)
	{
		//We set the image color
		Image image = this.gameObject.GetComponent<Image>();

		//And set the color. Easy
		image.color = targetColor;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
