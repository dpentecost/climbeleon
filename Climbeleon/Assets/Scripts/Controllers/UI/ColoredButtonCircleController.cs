﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ColoredButtonCircleController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	//Which color?
	public ColorHelper.COLORS_ENUM colorChoice;

	// Use this for initialization
	void Start()
	{
		
	}

	public void SetImageColor(Color targetColor)
	{
		//Get the button
		Image image = this.GetComponent<Image>();

		//Set it back
		image.color = targetColor;
	}

	// Update is called once per frame
	void Update()
	{
		//Set our own colors
		bool colorBlind = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_COLORBLIND);
		Color targetColor = ColorHelper.GetColorFromEnum(colorChoice, colorblind: colorBlind);

		//Now use that color
		this.SetImageColor(targetColor);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		//We entered the color. Pass in the value to the player
		GameObject playerObject = GameObject.FindGameObjectWithTag(ToolboxSingleton.TAG_PLAYER);

		//Get it's input controller
		PlayerInputController inputController = playerObject.GetComponent<PlayerInputController>();

		//Set the color
		inputController.ColorEntered(this.colorChoice);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		//We entered the color. Pass in the value to the player
		GameObject playerObject = GameObject.FindGameObjectWithTag(ToolboxSingleton.TAG_PLAYER);

		//Get it's input controller
		PlayerInputController inputController = playerObject.GetComponent<PlayerInputController>();

		//Set the color
		inputController.ColorExited(this.colorChoice);
	}
}
