﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MobileColorPanelController : MonoBehaviour {

	//The panel alpha
	public float panelAlpha = 127 / 255f;

	// Use this for initialization
	void Start () {

		//Set its color to match the 'safe' color
		Image panelImage = this.gameObject.GetComponent<Image>();

		//Set alpha
		Color panelColor = new Color(ColorHelper.colorSafe.r, ColorHelper.colorSafe.g, ColorHelper.colorSafe.b, panelAlpha);
		panelImage.color = panelColor;

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
