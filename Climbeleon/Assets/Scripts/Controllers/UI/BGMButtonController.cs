﻿using UnityEngine;

public class BGMButtonController : ToggleButtonController {

	public override Sprite GetCurrentSprite()
	{
		//Are we playing BGM?
		if (SettingsAndControls.IsLoaded())
		{
			if (SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_BGM))
			{
				//Use the play texture
				return this.spriteOn;
			}
			else
			{
				//We aren't playing
				return this.spriteOff;
			}
		}
		else
		{
			//Default to off
			return this.spriteOff;
		}
	}

	public override void OnToggle()
	{
		//Toggle the BGM
		AudioController.Instance.ToggleBGM();
	}

}
