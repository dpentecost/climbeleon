﻿using UnityEngine;
using System.Collections;

public class QuitButtonController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//If we are on mobile we don't want to have the button
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_EDITOR
		Destroy(this.gameObject);
#endif

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
