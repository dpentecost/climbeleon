﻿using UnityEngine;
using System.Collections;

public class ColorblindButtonController : ToggleButtonController {

	public override Sprite GetCurrentSprite()
	{
		//Are we Colorblind?
		if (SettingsAndControls.IsLoaded())
		{
			if (SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_COLORBLIND))
			{
				//Use the on texture
				return this.spriteOn;
			}
			else
			{
				//Nope
				return this.spriteOff;
			}
		}
		else
		{
			//Default to off
			return this.spriteOff;
		}
	}

	public override void OnToggle()
	{
		//Toggle the Colorblind
		ToolboxSingleton.Instance.ToggleColorblind();
	}

}
