﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleTextController : MonoBehaviour {

	//The title text for the game
	private string title = "CLIMBELEON";
	private ArrayList colors = new ArrayList();
	private ArrayList startColors;
	private ArrayList targetColors;

	//For changing the color
	private float changeTime;
	private bool wobbling = false;
	private float wobblingTime = 0f;
	private float titleChangeTime = 4f;
	//private float titleWobbleDistance = 10f;
	private float titleWobbleTime = 1f;


	// Use this for initialization
	void Start () {

		//Initialize random colors
		//Get the colors for each character
		foreach (char c in this.title) 
		{
			//Get a random color for it
			colors.Add(ColorHelper.GetRandomColor(getSafe: true));
		}

		//Start a timer
		this.changeTime = this.titleChangeTime;

	}
	
	// Update is called once per frame
	void Update () {

		//Update the timer
		this.changeTime -= Time.deltaTime;

		//Did we go under?
		if (this.changeTime <= 0)
		{
			//We need to move things and restart the timer
			this.changeTime = this.titleChangeTime;

			//We need to start changing
			this.wobbling = true;

			//Are we colorblind?
			bool colorBlind = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_COLORBLIND);

			//Now tween all the colors to new colors
			this.startColors = new ArrayList(this.colors);
			this.targetColors = new ArrayList();
			for (int i = 0; i < this.colors.Count; ++i)
			{
				//Make a new target
				this.targetColors.Add(ColorHelper.GetRandomColor(getSafe: true, colorblind: colorBlind));
			}
		}

		//Are we changing color?
		if (this.wobbling)
		{
			//We decrease the time
			this.wobblingTime += Time.deltaTime;

			//What percent through the wobble are we?
			float wobblePercent = this.wobblingTime / this.titleWobbleTime;

			//Set the colors
			for (int i = 0; i < this.targetColors.Count; ++i)
			{
				//We get the target color
				Color targetColor = (Color)this.targetColors[i];
				Color currentColor = (Color)this.startColors[i];

				//Now lerp it
				this.colors[i] = Color.Lerp(currentColor, targetColor, wobblePercent);
			}

			//Did we run out of time?
			if (this.wobblingTime >= this.titleWobbleTime)
			{
				//We're done wobbling
				this.wobbling = false;
				this.wobblingTime = 0;
			}
		}

		//We want to change the text to be the new rich text
		Text titleText = this.gameObject.GetComponent<Text>();

		//Set the text
		titleText.text = this.GenerateTitleRichText();
	}

	//Generate the rich text appropriately
	private string GenerateTitleRichText()
	{
		//The string to return
		string titleRichText = "";

		//For each character in the string
		for(int i = 0; i < this.title.Length; ++i)
		{
			//Get the character
			char currentCharacter = this.title[i];

			//We want to get the appropriate color...
			Color currentColor = (Color)this.colors[i];

			//Convert the color to HTML color
			string colorHex = ColorHelper.ColorToHex(currentColor);

			//Now build the string we will add for this character in the title
			string richCharacter = "<color=#" + colorHex + ">" + currentCharacter + "</color>";

			//Add to it
			titleRichText += richCharacter;
		}

		//Finally return it
		return titleRichText;
	}
}
