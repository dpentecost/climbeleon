﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof (Button))]
public class LeaderboardButtonController : MonoBehaviour {

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update () {
		//Is social ready?
		bool canLeaderboards = SocialSingleton.Instance.SocialIsReady() || SocialSingleton.Instance.CanSocial();
		if (canLeaderboards)
		{
			//We want to be a usable button
			Button leaderboardButton = this.gameObject.GetComponent<Button>();
			leaderboardButton.interactable = canLeaderboards;
		}
	}

	//When we want to view achievements
	public void ViewLeaderboards()
	{
		//Call the leaderboard boss

		//Are we ready to do the leaderboards?
		if (SocialSingleton.Instance.SocialIsReady())
		{
			SocialSingleton.Instance.ShowLeaderboard();
		}else if (SocialSingleton.Instance.CanSocial())
		{
			//We can't show the leaderboards but maybe we can log in instead
			SocialSingleton.Instance.AuthenticateUser();
		}
	}
}
