﻿using UnityEngine;
using System.Collections;

public class PlayerInputController : MonoBehaviour {

	//Our player controller
	private PlayerController player;
	
	//Variables for handling axis
	private bool jumpPressed = false;
	private bool jump1Pressed = false;
	private bool jump2Pressed = false;
	private bool jump3Pressed = false;
	private bool color1Pressed = false;
	private bool color2Pressed = false;
	private bool color3Pressed = false;

	//Variables for handling the touch interface
	public bool jumpGUIPressed = false;
	public bool color1GUIPressed = false;
	public bool color2GUIPressed = false;
	public bool color3GUIPressed = false;
	public ColorHelper.COLORS_ENUM lastEnteredColor = ColorHelper.COLORS_ENUM.SAFE;

	// Use this for initialization
	void Start () {
		//Get the player
		this.player = ToolboxSingleton.Instance.FindPlayerController();
	}
	
	// Update is called once per frame
	void Update () {

		//Handle the jump axis
		this.HandleJump();

		//Handle the color axes
		this.HandleColor();
	}

	//Handle pressing the GUI to jump
	public void JumpGUI(bool pressed)
	{
		//Save it
		this.jumpGUIPressed = pressed;

		//Tell the player to jump
		if (pressed)
		{
			this.player.JumpButtonDown();
		}
		else
		{
			this.player.JumpButtonReleased();
		}
	}

//	//Handle pressing the GUI to change color
//	public void ColorGUI(int colorIndex, bool pressed)
//	{
//		//Which color?
//		if(colorIndex == 0)
//		{
//			//Color 1
//			this.color1GUIPressed = pressed;
//			this.player.Color1Pressed();
//		}
//		else if(colorIndex == 1)
//		{
//			//Color 2
//			this.color2GUIPressed = pressed;
//			this.player.Color2Pressed();
//		}
//		else
//		{
//			//Anything else we assume its color 3
//			this.color3GUIPressed = pressed;
//			this.player.Color3Pressed();
//		}
//	}

	//Handle the jump button during the update loop
	private void HandleJump()
	{
		//Handle the regular jump
		this.HandleJumpAxis();

		//Handle all of the colorjumps
		this.HandleJump1Axis();
		this.HandleJump2Axis();
		this.HandleJump3Axis();

		//Handle telling that the jump was released
		if(ControllerHelper.GetAllJumpAxes() < 0.5f && !this.jumpGUIPressed)
		{
			//All jump buttons have been released
			//Tell the player
			this.player.JumpButtonReleased();
		}
	} 

	//Handle the regular jump axis
	private void HandleJumpAxis()
	{
		//Are we currently pressed?
		if (this.jumpPressed)
		{
			//We need to see if we are unpressing it
			if (ControllerHelper.GetJumpAxis() < 0.5f)
			{
				//We can assume this means we released it
				this.jumpPressed = false;
			}
		}
		else
		{
			//Did they press the button?
			if (ControllerHelper.GetJumpAxis() > 0.5f)
			{
				//The are pressing it.
				//Tell the user the jump button is pressed
				this.jumpPressed = true;

				//Tell the player 
				this.player.JumpButtonDown();
			}
		}
	}

	//Handle the regular jump axis
	private void HandleJump1Axis()
	{
		//Are we currently pressed?
		if (this.jump1Pressed)
		{
			//We need to see if we are unpressing it
			if (ControllerHelper.GetJump1Axis() < 0.5f)
			{
				//We can assume this means we released it
				this.jump1Pressed = false;
			}
		}
		else
		{
			//Did they press the button?
			if (ControllerHelper.GetJump1Axis() > 0.5f)
			{
				//The are pressing it.
				//Tell the user the jump button is pressed
				this.jump1Pressed = true;

				//Tell the player
				this.player.JumpButtonDown();

				//Also color
				this.player.ColorEntered(ColorHelper.COLORS_ENUM.COLOR1);
			}
		}
	}

	//Handle the regular jump axis
	private void HandleJump2Axis()
	{
		//Are we currently pressed?
		if (this.jump2Pressed)
		{
			//We need to see if we are unpressing it
			if (ControllerHelper.GetJump2Axis() < 0.5f)
			{
				//We can assume this means we released it
				this.jump2Pressed = false;
			}
		}
		else
		{
			//Did they press the button?
			if (ControllerHelper.GetJump2Axis() > 0.5f)
			{
				//The are pressing it.
				//Tell the user the jump button is pressed
				this.jump2Pressed = true;

				//Tell the player
				this.player.JumpButtonDown();

				//Also color
				this.player.ColorEntered(ColorHelper.COLORS_ENUM.COLOR2);
			}
		}
	}

	//Handle the regular jump axis
	private void HandleJump3Axis()
	{
		//Are we currently pressed?
		if (this.jump3Pressed)
		{
			//We need to see if we are unpressing it
			if (ControllerHelper.GetJump3Axis() < 0.5f)
			{
				//We can assume this means we released it
				this.jump3Pressed = false;
			}
		}
		else
		{
			//Did they press the button?
			if (ControllerHelper.GetJump3Axis() > 0.5f)
			{
				//The are pressing it.
				//Tell the user the jump button is pressed
				this.jump3Pressed = true;

				//Tell the player
				this.player.JumpButtonDown();

				//Also color
				this.player.ColorEntered(ColorHelper.COLORS_ENUM.COLOR3);
			}
		}
	}

	//When we press the colors
	public void ColorEntered(int color)
	{
		//Which color?
		ColorHelper.COLORS_ENUM targetColor = ColorHelper.GetColorEnumFromInt(color);

		this.ColorEntered(targetColor);	
	}
	//Pressed a color and sending a color enum
	public void ColorEntered(ColorHelper.COLORS_ENUM color)
	{
		//Were we already that color?
		if (color != this.lastEnteredColor)
		{
			//We are switching this color
			this.lastEnteredColor = color;

			//And switch the player
			this.player.ColorEntered(color);
		}
	}


	//When we exit a color
	public void ColorExited(int color)
	{
		//Which color?
		ColorHelper.COLORS_ENUM targetColor = ColorHelper.GetColorEnumFromInt(color);

		//Call the function that actually uses it
		this.ColorExited(targetColor);
	}
	//When we exit a color
	public void ColorExited(ColorHelper.COLORS_ENUM color)
	{
		//Use the color
	}

	//When we press a color
	public void ColorPressed(int color)
	{
		//Which color?
		ColorHelper.COLORS_ENUM targetColor = ColorHelper.GetColorEnumFromInt(color);

		//Tell the player that it was pressed
		this.ColorPressed(targetColor);

	}
	public void ColorPressed(ColorHelper.COLORS_ENUM color)
	{
		//Use the actual color
		this.player.ColorPressed(color);
	}

	//When we release a color button
	public void ColorReleased(int color)
	{
		//Which color?
		ColorHelper.COLORS_ENUM targetColor = ColorHelper.GetColorEnumFromInt(color);

		//Tell the player
		this.player.ColorReleased(targetColor);
	}

	//Handle the color input
	private void HandleColor()
	{
		//Get the compass index for hte axis
		int compassIndex = ControllerHelper.GetJoystickColorCompassIndex();
		if (!this.color1Pressed && ControllerHelper.GetColor1Axis() > 0 || ControllerHelper.GetCompassIndexColor1(compassIndex))
		{
			//We want to change color appropriately
			this.color1Pressed = true;
			this.color2Pressed = false;
			this.color3Pressed = false;
			this.player.ColorEntered(ColorHelper.COLORS_ENUM.COLOR1);
		}
		else if (!this.color2Pressed && ControllerHelper.GetColor2Axis() > 0 || ControllerHelper.GetCompassIndexColor2(compassIndex))
		{
			//We want to change color appropriately
			this.color1Pressed = false;
			this.color2Pressed = true;
			this.color3Pressed = false;
			this.player.ColorEntered(ColorHelper.COLORS_ENUM.COLOR2);
		}
		else if (!this.color3Pressed && ControllerHelper.GetColor3Axis() > 0 || ControllerHelper.GetCompassIndexColor3(compassIndex))
		{
			//We want to change color appropriately
			this.color1Pressed = false;
			this.color2Pressed = false;
			this.color3Pressed = true;
			this.player.ColorEntered(ColorHelper.COLORS_ENUM.COLOR3);
		}
	}
}
