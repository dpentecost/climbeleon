﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {

	//Static instance
	public static AudioController Instance = null;

	//The audio sources for BGM and SFX
	public AudioSource sourceSFX;
	public AudioSource sourceBGM;

	//The audio clips for the BGM
	public AudioClip menuBGM;
	public AudioClip gameBGM;

	//The audio clips for the SFX
	public AudioClip jumpSFX;
	public AudioClip crashSFX;
	public AudioClip burnSFX;
	public AudioClip fizzSFX;

	//Pitch variations
	public float lowPitchRange = 0.95f;
	public float highPitchRange = 1.05f;

	// Use for pre-initialization
	void Awake()
	{
		//Are we the first one?
		if (AudioController.Instance == null)
		{
			//Save ourselves
			AudioController.Instance = this;
		}
		else
		{
			//We are a duplicate. Go away
			Destroy(this.gameObject);
			return;
		}

		//Stay here
		DontDestroyOnLoad(this.gameObject);
	}

	// Use this for initialization
	void Start () {	
		
	}

	//Play a sound effect
	public void PlaySFX(AudioClip clipSFX)
	{
		//Are we allowed to play?
		if (!SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_SFX))
			return;

		//Pick a pitch offset
		this.sourceSFX.pitch = Random.Range(this.lowPitchRange, this.highPitchRange);

		//Set the clip and play
		this.sourceSFX.PlayOneShot(clipSFX);

	}

	//Loop a Background music
	public void LoopBGM(AudioClip clipBGM)
	{
		//Is it the clip that we're already playing?
		if (clipBGM == this.sourceBGM.clip)
		{
			//Do nothing in this case
			return;
        }
		else
		{
			//We want to set the clip and play
			this.sourceBGM.clip = clipBGM;

			//Are we allowed to play?
			if (SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_BGM))
			{
				//Looks like we are allowed to play!
				this.sourceBGM.Play();
			}			
		}
	}

	//Toggle SFX
	public void ToggleSFX()
	{
		//Simple
		bool playSFX = !SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_SFX);
        SettingsAndControls.Settings.SetSetting(ToolboxSingleton.PREF_PLAY_SFX,
			new SACBool(playSFX),
			Setting.SettingType.BOOLEAN);
	}

	//Toggle BGM
	public void ToggleBGM()
	{
		//Simple
		bool playBGM = !SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_BGM);
        SettingsAndControls.Settings.SetSetting(ToolboxSingleton.PREF_PLAY_BGM,
			new SACBool(playBGM),
			Setting.SettingType.BOOLEAN);

		//If Start/Stop BGM playing
		if (playBGM)
		{
			//Play whatever we got
			this.sourceBGM.Play();
		}
		else
		{
			//Don't play
			this.sourceBGM.Stop();
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
