﻿using UnityEngine;
using System.Collections;

public class ObstacleController : MonoBehaviour {

	//Obstacles have a color
	public ColorHelper.COLORS_ENUM color = ColorHelper.COLORS_ENUM.SAFE;

	//The player we are tracking
	private PlayerController player;

	//The max distance below the player before we die
	private float maxDistanceBelowPlayer = 20;

	// Use this for initialization
	void Start()
	{
		//Find the player
		this.player = GameObject.FindGameObjectWithTag(ToolboxSingleton.TAG_PLAYER).GetComponent<PlayerController>();
	}

	// Update is called once per frame
	void Update()
	{

		//Set the color
		bool colorBlind = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_COLORBLIND);
		this.GetComponent<SpriteRenderer>().color = ColorHelper.GetColorFromEnum(this.color, colorblind: colorBlind);

		//Are we too far away?
		if (this.transform.position.y + this.maxDistanceBelowPlayer < this.player.transform.position.y)
		{
			//We're too far away
			Destroy(this.gameObject);
		}
	}
}
