﻿using UnityEngine;
using System.Collections;

public class WallColliderController : MonoBehaviour {

	//Which side is this one?
	public bool isLeft = true;

	// Use this for initialization
	void Start () {

		//Dependong on which side this is...
		int side = 1;
		if (this.isLeft)
		{
			//We want to be on the correct side
			side = -1;
		}

		//Now position ourselves accordingly
		float xPos = ToolboxSingleton.Instance.TOWER_WIDTH / 2;
		//xPos += this.GetComponent<BoxCollider2D>().size.x / 2;
		xPos *= side;

		//And go there
		this.transform.position = new Vector3(xPos, this.transform.position.y, this.transform.position.y);

	}
	
	// Update is called once per frame
	void Update () {

	}
}
