﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class LaserController : MonoBehaviour
{

	//Sprites
	public Sprite onSprite;
	public Sprite offSprite;

	//Who is it chasing
	public GameObject player;

	//Is it on or off?
	public bool laserTriggered = false;
	public bool laserOn = false;

	//The timeout before we begin
	private int startWaitTime = 1;

	//Speed of vertical impulse
	private float speed = 7f;

	//The max distance away it can be
	private float maxDistance = 14.25f;

	//Some variables for the sound
	private float maxVolume = 0.125f;
	private float minFalloff = 3f;
	private float maxFalloff = 15f;

	// Use this for initialization
	void Start()
	{
		//Find the player
		this.player = GameObject.FindWithTag(ToolboxSingleton.TAG_PLAYER);
	}

	//Start the laser
	IEnumerator TurnOnLaser()
	{
		//We want to wait..
		this.laserTriggered = true;
		yield return new WaitForSeconds(this.startWaitTime);

		//Is the player underneath us?
		if (player.transform.position.y < this.transform.position.y + 1)
		{
			//Don't actually trigger
			this.laserTriggered = false;
		}
		else
		{
			//Turn to ON!
			this.GetComponent<SpriteRenderer>().sprite = this.onSprite;
			this.laserOn = true;

			//Give it a boost up
			//Apply the force
			Rigidbody2D rigidBody = this.GetComponent<Rigidbody2D>();
			ForceMode2D forceMode = ForceMode2D.Impulse;
			Vector2 force = new Vector2(0, this.speed);
			rigidBody.AddForce(force, forceMode);

			//Turn on the particles
			ParticleSystem particleSystem = this.GetComponent<ParticleSystem>();
			particleSystem.Play();
		}	
	}

	// Update is called once per frame
	void Update()
	{

		//Are we above it?
		if (this.laserOn && (player.transform.position.y - this.transform.position.y) > this.maxDistance)
		{
			//We're too far behind. Catch up
			this.transform.position = new Vector3(this.transform.position.x, player.transform.position.y - this.maxDistance, this.transform.position.z);
		}

		//Are we too far behind?
		if (!this.laserTriggered && player.transform.position.y >= this.transform.position.y)
		{
			//We need to start chasing... after a delay
			StartCoroutine(this.TurnOnLaser());
		}

		//Handle the sound
		this.HandleSound();
		
	}

	void HandleSound()
	{
		//Do we need to start or stop the music?
		AudioSource audioSource = this.GetComponent<AudioSource>();

		//Should it play?
		bool shouldPlay = this.laserOn && SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_PLAY_SFX);

		//Set it
		audioSource.mute = !shouldPlay;

		//What volume to play at?
		float volume = this.maxVolume;

		//Get the player's Y position vs the laser's
		float deltaY = Mathf.Abs(this.player.transform.localPosition.y - this.transform.localPosition.y);

		//Is it greater than the min?
		if(deltaY > this.minFalloff)
		{
			//Is it greater than the max?
			if(deltaY > this.maxFalloff)
			{
				//It's off
				volume = 0f;
			}
			else
			{
				//We need to scale it
				volume = (1 - ((deltaY - this.minFalloff) / (this.maxFalloff - this.minFalloff))) * this.maxVolume;
			}
		}

		//Set the volume
		audioSource.volume = volume;
	}
}
