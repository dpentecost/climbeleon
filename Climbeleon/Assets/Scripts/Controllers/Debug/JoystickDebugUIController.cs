﻿using UnityEngine;
using System.Collections;

public class JoystickDebugUIController : MonoBehaviour {

	//Get all the children appropriately
	private int arrowChildIndex = 0;
	private int targetChildIndex = 1;
	private int backgroundChildIndex = 2;
	private GameObject arrowChild;
	private GameObject targetChild;
	private GameObject backgroundChild;

	//Sizes of the background
	private float backgroundWidth;
	private float backgroundHeight;

	// Use this for initialization
	void Start () {

		//Are we even in debug?
		if (!ToolboxSingleton.DEBUG)
		{
			//Destroy
			Destroy(this.gameObject);
			return;
		}

		//Get all the children
		this.arrowChild = this.transform.GetChild(this.arrowChildIndex).gameObject;
		this.targetChild = this.transform.GetChild(this.targetChildIndex).gameObject;
		this.backgroundChild = this.transform.GetChild(this.backgroundChildIndex).gameObject;

		//Get the size
		this.backgroundWidth = this.backgroundChild.GetComponent<SpriteRenderer>().sprite.texture.width / 100f;
		this.backgroundHeight = this.backgroundChild.GetComponent<SpriteRenderer>().sprite.texture.height / 100f;

	}
	
	// Update is called once per frame
	void Update () {

		//Move the target around according to the joystick
		Vector2 joystickAxis = ControllerHelper.GetJoystickColor();

		//Move the target
		this.targetChild.transform.localPosition = new Vector3(joystickAxis.x * this.backgroundWidth / 2, joystickAxis.y * this.backgroundHeight / 2, this.targetChild.transform.localPosition.z);
	}
}
