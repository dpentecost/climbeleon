﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// http://wiki.unity3d.com/index.php/Toolbox
/// </summary>
public class ToolboxSingleton : Singleton<ToolboxSingleton> {
	protected ToolboxSingleton() { } // guarantee this will be always a singleton only - can't use the constructor!

	//Setup a bunch of global variables

	#region GLOBALS

	#region DEBUG
	//Are we on DEBUG?
	public static readonly bool DEBUG = false;
	public static readonly bool NO_GUI = false;
	#endregion

	#region SCENES
	//Various strings for scenes
	public static readonly string SCENE_MAIN = "main";
	public static readonly string SCENE_GAME = "game";
	public static readonly string SCENE_ATTRIBUTION = "attribution";
	#endregion

	#region PREFERENCES
	//Various keys for player prefs
	public static readonly string PREF_HIGH_SCORE = "HIGH_SCORE";
	public static readonly string PREF_PLAY_COUNT = "PLAY_COUNT";
	public static readonly string PREF_VIBRATE = "VIBRATE";
	public static readonly string PREF_PLAY_SFX = "PLAY_SFX";
	public static readonly string PREF_PLAY_BGM = "PLAY_BGM";
	public static readonly string PREF_COLORBLIND = "COLORBLIND";
	public static readonly string PREF_AD_COUNTER = "AD_COUNTER";
	#endregion

	#region TAGS
	public static readonly string TAG_PLAYER = "Player";
	public static readonly string TAG_GAMECONTROLLER = "GameController";
	#endregion

	#region ADVERTISEMENTS
	//ADVERTISEMENTS
	public static bool DISPLAY_ADVERTISEMENTS = true;
	public static int PLAYS_BEFORE_ADVERTISEMENTS = 10;
	public static string INTERSTITIAL_AD_ID = "ca-app-pub-4073679339672989/5356374952";
	public static string BANNER_AD_ID = "ca-app-pub-4073679339672989/9719195757";

	//AD KEYWORDS
	public static string[] AD_KEYWORDS = { "game", "gaming", "games", "arcade", "mobile" };
	#endregion

	#region SOCIAL
	//ANDROID
	public static string ANDROID_PACKAGE = "com.DEVTEKLLC.Climbeleon";
	public static string ANDROID_LEADERBOARD_ID = "CgkI_OCGudsaEAIQAQ";
	public static string ANDROID_ACHIEVEMENT_BLACK = "CgkI_OCGudsaEAIQAg";
	public static string ANDROID_ACHIEVEMENT_BLACK_EASY = "CgkI_OCGudsaEAIQCQ";
	public static string ANDROID_ACHIEVEMENT_BLACK_HARD = "CgkI_OCGudsaEAIQCw";
	public static string ANDROID_ACHIEVEMENT_COLOR_EASY = "CgkI_OCGudsaEAIQEQ";
	public static string ANDROID_ACHIEVEMENT_COLOR_HARD = "CgkI_OCGudsaEAIQEA";
	public static string ANDROID_ACHIEVEMENT_LASER = "CgkI_OCGudsaEAIQAw";
	public static string ANDROID_ACHIEVEMENT_LASER_EASY = "CgkI_OCGudsaEAIQDA";
	public static string ANDROID_ACHIEVEMENT_LASER_HARD = "CgkI_OCGudsaEAIQDQ";
	public static string ANDROID_ACHIEVEMENT_FALL_EASY = "CgkI_OCGudsaEAIQBw";
	public static string ANDROID_ACHIEVEMENT_FALL_HARD = "CgkI_OCGudsaEAIQCA";
	public static string ANDROID_ACHIEVEMENT_CLIMB_EASY = "CgkI_OCGudsaEAIQBA";
	public static int ANDROID_ACHIEVEMENT_CLIMB_EASY_REQUIRED = 200;
	public static string ANDROID_ACHIEVEMENT_CLIMB_MEDIUM = "CgkI_OCGudsaEAIQBQ";
	public static int ANDROID_ACHIEVEMENT_CLIMB_MEDIUM_REQUIRED = 500;
	public static string ANDROID_ACHIEVEMENT_CLIMB_HARD = "CgkI_OCGudsaEAIQBg";
	public static int ANDROID_ACHIEVEMENT_CLIMB_HARD_REQUIRED = 1000;
	public static string ANDROID_ACHIEVEMENT_CLIMB_IMPOSSIBLE = "CgkI_OCGudsaEAIQDg";
	public static int ANDROID_ACHIEVEMENT_CLIMB_IMPOSSIBLE_REQUIRED = 1500;
	public static string ANDROID_ACHIEVEMENT_TOTAL = "CgkI_OCGudsaEAIQDw";
	public static string ANDROID_EVENT_CLIMB = "CgkI_OCGudsaEAIQEw";
	public static string ANDROID_EVENT_FALL = "CgkI_OCGudsaEAIQFA";

	#endregion

	#region CONSTANTS
	//Level construction constants
	public readonly float TOWER_WIDTH = 12;
	public readonly float OBSTACLE_REGION_WIDTH = 4;

	//Camera constants
	public readonly float MIN_CAMERA_POSISITON = 3;
	public readonly float CAMERA_DAMPING_FACTOR = 8;
	#endregion


	#endregion

	//Instance variables
	#region INSTANCE VARIABLES
	//The fade helper
	FadeHelper fadeHelper;
	#endregion
	void Awake()
	{
		// Your initialization code here

		//Create a fade helper
		ToolboxSingleton.RegisterComponent<FadeHelper>();
	}

	// (optional) allow runtime registration of global objects
	static public T RegisterComponent<T>() where T : Component
	{
		return Instance.GetOrAddComponent<T>();
	}

	#region METHODS

	#region SETTINGS
	//Getting and Setting the score, of course
	public int GetHighScore()
	{
		//Get it from the settings, its pretty easy
		return SettingsAndControls.Settings.GetInt(ToolboxSingleton.PREF_HIGH_SCORE);
	}
	public bool SetHighScore(int score, bool ignoreHighScore = false)
	{
		//Lets check the current high score
		int currentScore = this.GetHighScore();

		//DebugPrint the scores
		if (ToolboxSingleton.DEBUG)
			print("New Score: " + score + " Old Score: " + currentScore);

		//Did we beat it? Or are we setting regardless?
		if(score > currentScore || ignoreHighScore)
		{
			//We can set it
			SettingsAndControls.Settings.SetSetting(ToolboxSingleton.PREF_HIGH_SCORE,
				new SACInt(score),
				Setting.SettingType.INTEGER);

			//And return that it was set
			return true;
		}

		//We didn't set anything because we didn't beat the score.
		return false;
	}

	//Getting and Setting the play count, so we wait for ads
	public int GetPlayCount()
	{
		//Get it from the settings, its pretty easy
		return SettingsAndControls.Settings.GetInt(ToolboxSingleton.PREF_PLAY_COUNT);
	}
	public bool IncrementPlayCount()
	{
		//Lets check the current play count and f
		int oldPlayCount = this.GetPlayCount();
		int newPlayCount = oldPlayCount + 1;

		//DebugPrint the scores
		if (ToolboxSingleton.DEBUG)
			print("New Play Count: " + newPlayCount + " Old Play Count: " + oldPlayCount);

		//We can set it
		SettingsAndControls.Settings.SetSetting(ToolboxSingleton.PREF_PLAY_COUNT,
			new SACInt(newPlayCount),
			Setting.SettingType.INTEGER);

		//And return that it was set
		return true;
	}
	public bool PlayerIsReadyForAdvertisements()
	{
		//We compare the current play count to the expected
		int currentPlayCount = this.GetPlayCount();
		if(currentPlayCount > ToolboxSingleton.PLAYS_BEFORE_ADVERTISEMENTS)
		{
			//We can do ads now
			return true;
		}else
		{
			//We cannot do advertisements. Let them play a bit more
			return false;
		}
	}

	//Toggling colorblind mode
	public bool ToggleColorblind()
	{
		//What is the current status?
		bool colorBlind = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_COLORBLIND);

		//Set to the inverse
		SettingsAndControls.Settings.SetSetting(ToolboxSingleton.PREF_COLORBLIND,
			new SACBool(!colorBlind),
			Setting.SettingType.BOOLEAN);

		//Return the current state
		return !colorBlind;
	}
	//Toggling Vibration
	public bool ToggleVibration()
	{
		//What is the current status?
		bool vibration = SettingsAndControls.Settings.GetBool(ToolboxSingleton.PREF_VIBRATE);

		//Set to the inverse
		SettingsAndControls.Settings.SetSetting(ToolboxSingleton.PREF_VIBRATE,
			new SACBool(!vibration),
			Setting.SettingType.BOOLEAN);

		//Return the current state
		return !vibration;
	}
	#endregion

	#region HELPER FUNCTIONS
	public PlayerController FindPlayerController()
	{
		//Get the playercontroller by tag and return it
		return GameObject.FindGameObjectWithTag(ToolboxSingleton.TAG_PLAYER).GetComponent<PlayerController>();
	}
	public GameController FindGameController()
	{
		//Get the Game Controller by tag and return it
		return GameObject.FindGameObjectWithTag(ToolboxSingleton.TAG_GAMECONTROLLER).GetComponent<GameController>();
	}
	#endregion

	#region GAME FLOW
	//When the user wants to quit
	public void QuitGame()
	{
		//Just quit
		Application.Quit();
	}

	//Fade to black
	public IEnumerator FadeToBlack()
	{
		//Get the fade handler
		FadeHelper fadeHelper = this.GetComponent<FadeHelper>();

		//Figure out how long to wait
		float waitTime = fadeHelper.BeginFade(FadeHelper.FADE_OUT);

		//Now wait that long
		yield return new WaitForSeconds(waitTime);
	}

	//Handle fading out
	private IEnumerator FadeOut(string targetScene)
	{
		//Wait for the fade to black
		yield return this.FadeToBlack();

		//Now load the scene
		SceneManager.LoadScene(targetScene);
	}

	public void FadeToScene(string targetScene)
	{
		//Start the coroutine
		StartCoroutine(this.FadeOut(targetScene));
	}

	//When we quit
	void OnApplicationQuit()
	{
		//Save settings
		SettingsAndControls.Save();
	}
	#endregion

	#region ADVERTISEMENTS

	//Getting the ad IDs
	public static string GetInterstitialAdID()
	{
		//Get the AD id based on platform
#if UNITY_EDITOR
		string adUnitID = "unused";
#elif UNITY_ANDROID
         string adUnitID = ToolboxSingleton.INTERSTITIAL_AD_ID;
#elif UNITY_IPHONE
         string adUnitID = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
#else
         string adUnitID = "unexpected_platform";
#endif
		//Return the ID
		return adUnitID;
	}
	public static string GetBannerAdID()
	{
		//Get the AD id based on platform
#if UNITY_EDITOR
		string adUnitID = "unused";
#elif UNITY_ANDROID
         string adUnitID = ToolboxSingleton.BANNER_AD_ID;
#elif UNITY_IPHONE
         string adUnitID = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
         string adUnitID = "unexpected_platform";
#endif
		//Return the ID
		return adUnitID;
	}

	#endregion

	#endregion
}

[System.Serializable]
public class Language
{
	public string current;
	public string lastLang;
}