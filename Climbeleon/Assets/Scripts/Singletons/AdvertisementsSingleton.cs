﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;

public class AdvertisementsSingleton : Singleton<AdvertisementsSingleton> {

	protected AdvertisementsSingleton() { } // guarantee this will be always a singleton only - can't use the constructor!

	//State of the enum
	public enum InterstitialState { NOT_READY, READY, WAITING, FAILED };
	private InterstitialState interstitialState = InterstitialState.NOT_READY;

	#region INSTANCE VARIABLES
	//The current interstitial and banner ad
	private InterstitialAd interstitialAd;
	private BannerView bannerView;
	#endregion

	// Use this for initialization
	void Start () {

		//Do we request anything?
		if (ToolboxSingleton.DISPLAY_ADVERTISEMENTS)
		{
			//We need to request both an interstitial (For the future) and a banner (whenever it's ready)
			this.RequestInterstitial();
			this.SetupBanner();
			this.RequestBanner();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//Create an ad request
	private AdRequest CreateAdRequest()
	{
		// Create an empty ad request.
		AdRequest.Builder requestBuilder = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator);

		//Add all the keywords
		foreach (string keyword in ToolboxSingleton.AD_KEYWORDS)
		{
			//Add it
			requestBuilder.AddKeyword(keyword);
		}

		//Return the built request
		return requestBuilder.Build();
	}

	#region INTERSTITIAL
	//Request the interstitial
	private void RequestInterstitial()
	{
		//Do we even want ads?
		if (!ToolboxSingleton.DISPLAY_ADVERTISEMENTS)
		{
			//We're never ready
			this.interstitialState = InterstitialState.NOT_READY;
			return;
		}

		//Decide what platform we're on
		string adUnitID = ToolboxSingleton.GetInterstitialAdID();

		//Create the interstitial
		this.interstitialAd = new InterstitialAd(adUnitID);

		//Listen for events from the ad
		this.interstitialAd.OnAdLoaded				+= this.HandleInterstitialLoaded;
		this.interstitialAd.OnAdOpening				+= this.HandleInterstitialOpening;
		this.interstitialAd.OnAdFailedToLoad		+= this.HandleInterstitialFailed;
		this.interstitialAd.OnAdClosed				+= this.HandleInterstitialClosed;
		this.interstitialAd.OnAdLeavingApplication	+= this.HandleInterstitialLeftApplication;

		// Load an interstitial ad.
		this.interstitialState = InterstitialState.WAITING;
		this.interstitialAd.LoadAd(this.CreateAdRequest());
	}

	//Add an 'on close' to the ad
	public void AddOnAdClosed(System.EventHandler<EventArgs> targetHandler)
	{
		//Add it
		this.interstitialAd.OnAdClosed += targetHandler;
	}

	//Is it ready?
	public bool InterstitialIsReady()
	{
		//Simple

		//If it isn't ready, maybe we should try again
		if (this.interstitialState == InterstitialState.FAILED)
		{
			//Lets try again
			this.RequestInterstitial();
		}

		//Is it in the ready state?
		return this.interstitialState == InterstitialState.READY;
	}

	//Display the ad
	public void DisplayInterstitialAd()
	{
		//Simply display if it's ready, 
		if (this.interstitialState == InterstitialState.READY)
		{
			this.interstitialAd.Show();
		}
		else
		{
			//We must need to just load another one
			this.RequestInterstitial();
		}
	}

	/**
		Various handlers for Interstitial ads
	**/
	#region Interstitial Event Handlers
	private void HandleInterstitialLoaded(object sender, EventArgs args)
	{
		//What to do when the ad loads

		//If anyone is waiting, we're ready to display
		this.interstitialState = InterstitialState.READY;
	}
	private void HandleInterstitialOpening(object sender, EventArgs args)
	{
		//What to do when the ad is being opened
	}
	private void HandleInterstitialFailed(object sender, EventArgs args)
	{
		//What to do when the ad fails

		//Set the state
		this.interstitialState = InterstitialState.FAILED;
	}
	private void HandleInterstitialClosed(object sender, EventArgs args)
	{
		//What to do when the ad closes

		//If anyone is waiting, we're not ready to display
		this.interstitialState = InterstitialState.NOT_READY;

		//Request a new one
		this.RequestInterstitial();
	}
	private void HandleInterstitialLeftApplication(object sender, EventArgs args)
	{
		//What to do when the user follows the ad
	}
	#endregion

	#endregion


	#region BANNER
	//Prepare the banner
	private void SetupBanner()
	{
		//Do we even want ads?
		if (!ToolboxSingleton.DISPLAY_ADVERTISEMENTS)
		{
			//We're never ready
			return;
		}

		//Get the ad unit
		string adUnitID = ToolboxSingleton.GetBannerAdID();

		//Create the view
		this.bannerView = new BannerView(adUnitID, AdSize.SmartBanner, AdPosition.Bottom);
	}
	//Get a new banner
	private void RequestBanner()
	{
		//Do we even want ads?
		if (!ToolboxSingleton.DISPLAY_ADVERTISEMENTS)
		{
			//We're never ready
			return;
		}

		//Create the request for the ad
		AdRequest adRequest = this.CreateAdRequest();

		//And load the ad
		this.bannerView.LoadAd(adRequest);
	}
	//Show or hide
	public void SetBannerVisibility(bool visible)
	{
		//Are we even doing ads?
		if (!ToolboxSingleton.DISPLAY_ADVERTISEMENTS)
		{
			//We're never ready
			return;
		}

		//Set the visibility
		if (visible)
			this.bannerView.Show();
		else
			this.bannerView.Hide();

	}
	#endregion
}
