﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;

#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class SocialSingleton : Singleton<SocialSingleton> {

	protected SocialSingleton() { } // guarantee this will be always a singleton only - can't use the constructor!

	//Can we even do social?
	private bool socialReady = false;
	private bool canSocial = false; //Assume we can't do social

	// Use this for initialization
	void Start () {
		//Setup our social stuff
		this.SetupSocial();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//Setup the social lines
	private void SetupSocial()
	{
		//Are we on google?
#if UNITY_ANDROID

		//We can do social in general
		this.canSocial = true;
				
		//Are we debugging?
		if (ToolboxSingleton.DEBUG)
		{
			//We want debug logging then
			PlayGamesPlatform.DebugLogEnabled = true;
		}
		//Activate the platform and authenticate the user
		PlayGamesPlatform.Activate();

		//Attempt to authenticate
		this.AuthenticateUser();

#endif
	}

	public void AuthenticateUser()
	{
		//Attempt to authenticate the user
		Social.Active.localUser.Authenticate((bool success) =>
		{
			//Was it a success?
			this.socialReady = success;
			print("Android Authentification Result: " + success);

			//Lets also nab the high score if it was a success
			if (success)
			{
				//Get info from the leaderboard
				this.RetrieveScoreAndroid();
			}
		});
	}


	//Is social ready to go?
	public bool SocialIsReady()
	{
		//Simply return the bool
		return this.socialReady;
	}

	//Can we social?
	public bool CanSocial()
	{
		return this.canSocial;
	}

	#region SCORE
	//Reporting a score. Return true if it was a successful high score
	public bool ReportScore(int score)
	{
		//Are we ready?
		if (socialReady)
		{
			//Update leaderboards

			//Are we on android?
#if UNITY_ANDROID
			this.ReportScoreAndroid(score);
#endif
		}

		//Set our high score
		bool newHighScore = ToolboxSingleton.Instance.SetHighScore(score);
		return newHighScore;
	}

	//Report for android
	private void ReportScoreAndroid(int score)
	{
		//We are going to try to commit our score
		Social.ReportScore(score, ToolboxSingleton.ANDROID_LEADERBOARD_ID, (bool success) =>
		{
			//Handle yourself
			print("High Score success: " + success);
		});

		//At least 200?
		if (score >= ToolboxSingleton.ANDROID_ACHIEVEMENT_CLIMB_EASY_REQUIRED)
			Social.ReportProgress(ToolboxSingleton.ANDROID_ACHIEVEMENT_CLIMB_EASY, 100f, (bool success) =>
			{
				//Did it?
				print("Score Easy: " + success);
			});
		if (score >= ToolboxSingleton.ANDROID_ACHIEVEMENT_CLIMB_MEDIUM_REQUIRED)
			Social.ReportProgress(ToolboxSingleton.ANDROID_ACHIEVEMENT_CLIMB_MEDIUM, 100f, (bool success) =>
			{
				//Did it?
				print("Score Medium: " + success);
			});
		if (score >= ToolboxSingleton.ANDROID_ACHIEVEMENT_CLIMB_HARD_REQUIRED)
			Social.ReportProgress(ToolboxSingleton.ANDROID_ACHIEVEMENT_CLIMB_HARD, 100f, (bool success) =>
			{
				//Did it?
				print("Score Hard: " + success);
			});
		if (score >= ToolboxSingleton.ANDROID_ACHIEVEMENT_CLIMB_IMPOSSIBLE_REQUIRED)
			Social.ReportProgress(ToolboxSingleton.ANDROID_ACHIEVEMENT_CLIMB_IMPOSSIBLE, 100f, (bool success) =>
			{
				//Did it?
				print("Score Impossible: " + success);
			});
	}

	//Get the user's score
	private void RetrieveScoreAndroid()
	{
#if UNITY_ANDROID || UNITY_IPHONE
		//Get the Leaderboard
		ILeaderboard leaderboard = PlayGamesPlatform.Instance.CreateLeaderboard();

		//Set up the leaderboard
		string[] userIDs = { Social.localUser.id };
        leaderboard.id = ToolboxSingleton.ANDROID_LEADERBOARD_ID;
		leaderboard.SetUserFilter(userIDs);

		//Now load the scores and set the high score
		leaderboard.LoadScores(leaderboardResult =>
		{
			//Was there a result?
			if (leaderboardResult)
			{
				//We want to set our current 'high score'
				int score = (int)leaderboard.localUserScore.value;
				ToolboxSingleton.Instance.SetHighScore(score);
            }
		});
#endif
	}
	#endregion

	#region EVENTS
	//Incrementing an event
	private void IncrementEvent(string eventName, int amount)
	{
#if UNITY_ANDROID || UNITY_IPHONE
		//Are we not ready?
		if (!socialReady)
			return;

		//Increment the event
		PlayGamesPlatform.Instance.Events.IncrementEvent(eventName, (uint)amount);
#endif
	}
	//Incrementing the climb event
	public void IncrementClimbEvent(int score)
	{
		//Send the score off
		this.IncrementEvent(ToolboxSingleton.ANDROID_EVENT_CLIMB, score);
	}
	//Increment the fall event
	public void IncrementFallEvent()
	{
		//We just fell so we add 1
		this.IncrementEvent(ToolboxSingleton.ANDROID_EVENT_FALL, 1);
	}
	
#endregion

#region ACHIEVEMENTS
	//Simply show the achievements
	public void ShowAchievements()
	{
		//Are we not ready?
		if (!socialReady)
			return;

		//Use social
		Social.ShowAchievementsUI();
	}

	//Increment an achievement some amount
	private void IncrementAchievement(string achievementID, int amount)
	{
		//Are we not ready?
		if (!socialReady)
			return;

		//Are we on android?
#if UNITY_ANDROID
		//Use the android Play Games platform
		PlayGamesPlatform.Instance.IncrementAchievement(achievementID, amount, (bool success) =>
		{
			//Print if it was a success?
			print("Achievement Increment Status: " + achievementID + " " + amount + " " + success);
		});
#endif
	}

	//Unlock an achievement
	private void UnlockAchievement(string achievementID)
	{
		//Are we not ready?
		if (!socialReady)
			return;

		//Are we on android?
#if UNITY_ANDROID
		//Use the android Play Games platform
		Social.ReportProgress(achievementID, 100f, (bool success) =>
		{
			//Print if it was a success?
			print("Achievement Unlock Status: " + achievementID + " " + success);
		});
#endif
	}

	public void IncrementClimbAchievement(int height)
	{
		//The user climbed. Lets increment it
		this.IncrementAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_TOTAL, height);
	}

	public void IncrementFallAchievement()
	{
		//The user lost. Increment the falling achievements
		this.IncrementAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_FALL_EASY, 1);
		this.IncrementAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_FALL_HARD, 1);
	}

	public void IncrementBlackTileAchievement()
	{
		//The user collided with a black tile. Increment the social achievement for it
		this.UnlockAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_BLACK);
		this.IncrementAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_BLACK_EASY, 1);
		this.IncrementAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_BLACK_HARD, 1);
	}

	public void IncrementLaserAchievement()
	{
		//The user collided with the laser
		this.UnlockAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_LASER);
		this.IncrementAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_LASER_EASY, 1);
		this.IncrementAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_LASER_HARD, 1);
	}

	public void IncrementColorAchievement()
	{
		//The user collided with the wrong color
		this.IncrementAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_COLOR_EASY, 1);
		this.IncrementAchievement(ToolboxSingleton.ANDROID_ACHIEVEMENT_COLOR_HARD, 1);
	}
#endregion

#region LEADERBOARD
	//Simply show the leaderboard
	public void ShowLeaderboard()
	{
		//Use social
		Social.ShowLeaderboardUI();
	}
#endregion
}
