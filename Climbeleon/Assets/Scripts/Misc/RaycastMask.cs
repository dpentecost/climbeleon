﻿using UnityEngine;
using UnityEngine.UI;

//Requre a location and a mask
[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Image))]
public class RaycastMask : MonoBehaviour, ICanvasRaycastFilter
{
	//The sprite to use
	private RectTransform rectTransform;
	private Sprite maskingSprite;

	void Start()
	{
		//Get the image component's sprite for use on the mask
		this.rectTransform = this.transform as RectTransform;
		this.maskingSprite = GetComponent<Image>().sprite;
	}

	#region ICanvasRaycastFilter implementation

	public bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
	{
		// Get normalized hit point within rectangle (aka UV coordinates originating from bottom-left)
		Vector2 rectPoint;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, screenPoint, eventCamera, out rectPoint);
		Vector2 normPoint = (rectPoint - rectTransform.rect.min);
		normPoint.x /= rectTransform.rect.width;
		normPoint.y /= rectTransform.rect.height;

		// Read pixel color at normalized hit point
		Texture2D texture = maskingSprite.texture;
		Color color = texture.GetPixel((int)(normPoint.x * texture.width), (int)(normPoint.y * texture.height));

		// Filter away hits on transparent pixels
		return color.a > 0f;
	}

	#endregion
}
