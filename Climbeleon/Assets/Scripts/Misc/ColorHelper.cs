﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorHelper {

	//Enum of all colors
	//public static string[] COLORS = new[] { "COLORBACKGROUND", "COLORSAFE", "COLORDANGEROUS", "COLOR1", "COLOR2", "COLOR3" };
	public enum COLORS_ENUM {
		PANEL,
		BACKGROUND,
		SAFE,
		DANGEROUS,
		COLOR1,
		COLOR2,
		COLOR3,
	}

	//Possible colors
	private static float CN = 255f;
	public static readonly Color colorPanel			= new Color(071 / CN, 071 / CN, 071 / CN);
	public static readonly Color colorBackground	= new Color(208 / CN, 208 / CN, 208 / CN);
	public static readonly Color colorSafe			= new Color(241 / CN, 247 / CN, 247 / CN);
	public static readonly Color colorDangerous		= new Color(000 / CN, 000 / CN, 000 / CN);
	public static readonly Color color1				= new Color(227 / CN, 092 / CN, 092 / CN);
	public static readonly Color color2				= new Color(110 / CN, 188 / CN, 102 / CN);
	public static readonly Color color3				= new Color(071 / CN, 136 / CN, 163 / CN);
	public static readonly Color color1Alt			= new Color(213 / CN, 094 / CN, 000 / CN);
	public static readonly Color color2Alt			= new Color(240 / CN, 228 / CN, 066 / CN);
	public static readonly Color color3Alt			= new Color(086 / CN, 180 / CN, 223 / CN);

	//Get a random color
	public static Color GetRandomColor(bool getSafe = false, bool colorblind = false)
	{
		//Add them all to a list
		ArrayList colors = new ArrayList();

		//Are we colorblind?
		if (!colorblind)
		{
			//Add regular
			colors.Add(color1);
			colors.Add(color2);
			colors.Add(color3);
		}
		else
		{
			//Add colorblind
			colors.Add(color1Alt);
			colors.Add(color2Alt);
			colors.Add(color3Alt);
		}

		

		//Are we adding the safe?
		if (getSafe)
		{
			//We add the safe color as well
			colors.Add(colorSafe);
		}

		//Now pick a random number and get the color there
		return (Color)colors[Random.Range(0, colors.Count)];
	}

	//Some color converters for convenience
	public static string ColorToHex(Color color)
	{
		//Get all the values as int
		int a = (int)(color.a * 255);
		int r = (int)(color.r * 255);
		int g = (int)(color.g * 255);
		int b = (int)(color.b * 255);

		//Make the hex value and return it
		string colorHex = string.Format("{0:X2}{1:X2}{2:X2}{3:X2}", r, g, b, a);
		return colorHex;
	}

	//Helps you set a transition for a button with a target color
	public static ColorBlock GenerateColorBlock(ColorBlock originalBlock, Color targetColor)
	{
		//We take the old block and set some status

		//Hovered
		originalBlock.highlightedColor = targetColor;

		//Hovered
		targetColor.a = 0.8f;
		originalBlock.normalColor = targetColor;

		//Pressed
		targetColor.a = 0.5f;
		originalBlock.pressedColor = targetColor;

		//Disabled
		targetColor.a = 0.2f;
		originalBlock.disabledColor = targetColor;

		//And return it
		return originalBlock;	
	}

	//Gets the color from the enum
	public static Color GetColorFromEnum(COLORS_ENUM colorChoice, bool colorblind = false)
	{
		Color targetColor;
		//Check what color we have. Use colorblind if needed
		if (colorChoice == ColorHelper.COLORS_ENUM.BACKGROUND)
		{
			targetColor = ColorHelper.colorBackground;
		}
		else if (colorChoice == ColorHelper.COLORS_ENUM.SAFE)
		{
			targetColor = ColorHelper.colorSafe;
		}
		else if (colorChoice == ColorHelper.COLORS_ENUM.DANGEROUS)
		{
			targetColor = ColorHelper.colorDangerous;
		}
		else if (colorChoice == ColorHelper.COLORS_ENUM.COLOR1)
		{
			if (!colorblind)
			{
				targetColor = ColorHelper.color1;
			}
			else
			{
				targetColor = ColorHelper.color1Alt;
			}
			
		}
		else if (colorChoice == ColorHelper.COLORS_ENUM.COLOR2)
		{
			if (!colorblind)
			{
				targetColor = ColorHelper.color2;
			}
			else
			{
				targetColor = ColorHelper.color2Alt;
			}
		}
		else if (colorChoice == ColorHelper.COLORS_ENUM.COLOR3)
		{
			if (!colorblind)
			{
				targetColor = ColorHelper.color3;
			}
			else
			{
				targetColor = ColorHelper.color3Alt;
			}
			
		}
		else if (colorChoice == ColorHelper.COLORS_ENUM.PANEL)
		{
			targetColor = ColorHelper.colorPanel;
		}
		else
		{
			//Just use safe
			targetColor = ColorHelper.colorSafe;
		}

		//Return the target
		return targetColor;
	}

	public static ColorHelper.COLORS_ENUM GetColorEnumFromInt(int color)
	{
		//Which color?
		ColorHelper.COLORS_ENUM targetColor = ColorHelper.COLORS_ENUM.SAFE;
		if (color == 1)
		{
			//Change the color
			targetColor = ColorHelper.COLORS_ENUM.COLOR1;
		}
		else if (color == 2)
		{
			//Change the color
			targetColor = ColorHelper.COLORS_ENUM.COLOR2;
		}
		else if (color == 3)
		{
			//Change the color
			targetColor = ColorHelper.COLORS_ENUM.COLOR3;
		}

		//Return it
		return targetColor;
	}

}
