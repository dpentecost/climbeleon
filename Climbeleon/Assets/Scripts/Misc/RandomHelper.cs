﻿
//A helper for the System.Random class
//Basically just extends it
public class RandomHelper : System.Random {

	//Don't do anything fancy with the constructors
	public RandomHelper() : base() { }
	public RandomHelper(int Seed) : base(Seed) { }

	//Get a range >= minValue and < maxValue
	public double NextDouble(double minValue, double maxValue)
	{
		//Get which point in the range we're gonna be
		double rangeFraction = this.NextDouble();

		//Now place ourselves in the range
		double range = maxValue - minValue;
		double place = range * rangeFraction;

		//Finally, return ourselves that much further from the minValue
		double result = minValue + place;
		return result;
	}

	//Get a range >= minValue and < maxValue
	public float NextFloat(float minValue, float maxValue)
	{
		//Get which point in the range we're gonna be
		float rangeFraction = this.NextFloat();

		//Now place ourselves in the range
		float range = maxValue - minValue;
		float place = range * rangeFraction;

		//Finally, return ourselves that much further from the minValue
		float result = minValue + place;
		return result;
	}

	//Get a range >= minValue and < maxValue
	public float NextFloat()
	{
		//Just get a double and cast it
		float result = (float)this.NextDouble();
		return result;
	}

}
